program GluonPropRatios
  use Kinds
  use FileIO
  use MatrixOps
  use Gaugefield
  use GluonOps
  use iso_c_binding
  implicit none

  ! Link variables split into real and imaginary parts
  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: UR, UI

  ! Gluon fields
  complex(DC), dimension(:,:,:,:,:,:,:), allocatable :: momglu

  ! Propagator
  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: mprop_cutoff, mprop_jack
  real(DP), dimension(:,:,:,:,:,:), allocatable :: mprop

  integer :: icfg

  ! Variables for ratio calculation
  integer :: maxmom, n_combinations
  integer, dimension(:, :), allocatable :: comb
  real(DP), dimension(:, :), allocatable :: ratios
  character(len=3) :: momtype

  ! Constant - twice the trace
  real(DP) :: coeff

  ! File I/O
  real(DP) :: beta, meanlink
  character(len=256) :: filename, file_list
  character(len=256) :: out_dir
  character(len=4)   :: the_format
  integer :: nconfig
  integer, parameter :: infl = 204, otfl = 205

  ! **************************************************
  ! Ask for input
  ! **************************************************

  write(*,*) "What is the name of the file containing the input configurations?"
  read(*,*) file_list
  write(*,*) trim(file_list)

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt

  write(*, *) "What is the name of the gluon propagator ratio output directory?"
  read(*,*) out_dir
  write(*,*) trim(out_dir)

  write(*,*) "What is the file format?"
  read(*,*) the_format

  write(*,*) 'Please enter the maximum momentum to consider for ratio calculations.'
  read(*, *) maxmom
  write(*,*) maxmom

  write(*,*) 'Please enter the momentum type to use.'
  write(*,*) 'Options are: Wilson ("wil"), Luscher-Weisz ("lw") or uncorrected("")'
  read(*, *) momtype
  write(*,*) momtype

  write(*,*)

  call flushout()

  V = nx * ny * nz * nt
  q_scale(1) = 2.0 * pi / nx
  q_scale(2) = 2.0 * pi / ny
  q_scale(3) = 2.0 * pi / nz
  q_scale(4) = 2.0 * pi / nt
  ldims = [nx, ny, nz, nt]
  call get_ratio_combinations(maxmom, n_combinations, comb)

  ! Get number of configurations
  open(infl, file=trim(file_list), status="old", action="read")
  read(infl,*) nconfig

  ! |------------------------ Allocate arrays -------------------------|

  allocate(UR(NX,NY,NZ,NT,ND,NC,NC), UI(NX,NY,NZ,NT,ND,NC,NC))
  allocate(momglu(0:nx-1,0:ny-1,0:nz-1,0:nt-1,nd,nc,nc))
  allocate(mprop(0:NX - 1,0:NY - 1,0:NZ - 1,0:NT - 1, nd, nd))
  allocate(mprop_cutoff(0:maxmom, 0:maxmom, 0:maxmom, 0:maxmom, nd, nd, nconfig))
  allocate(mprop_jack(0:maxmom, 0:maxmom, 0:maxmom, 0:maxmom, nd, nd, 0:nconfig))


  coeff = 2.0d0 / real(V * (NC**2 - 1), DP)

  do icfg = 1, nconfig
    read(infl, '(a)') filename
    write(*,*) "Reading ", trim(filename)
    ! Read in the gaugefield
    if(the_format=="cssm") then
      ! For double precision configurations in compact form
      call ReadGaugeField_cssm(trim(filename), ur, ui, meanlink, beta)
    else if(the_format=="ildg") then
      call ReadGaugeField_ildg(trim(filename), ur, ui, meanlink, beta)
    else
      write(*,*) "Unrecognised format, aborting"
      stop
    end if

    ! Calculate the gluon field
    call calc_Amu(UR, UI, meanlink)

    ! Calculate the momentum-space gluon field
    call calc_mom_Amu(UR, UI, momglu)

    ! Calculate the tensor propagator for each configuration
    call calc_tensor_prop(momglu, coeff, mprop)

    ! Consider only momenta with components less than maxmom
    mprop_cutoff(:, :, :, :, :, :, icfg) = mprop(0:maxmom, 0:maxmom, 0:maxmom, 0:maxmom, :, :)

  end do
  ! ------------------ Calculate gluon prop ratios -------------------!!

  call get_jackknife_samples(mprop_cutoff, mprop_jack)
  call analyse_ratios(mprop_jack, comb, momtype, ratios)
  call write_ratios(otfl, out_dir, ratios, comb)
  write(*,*) "Finished"

contains
  subroutine get_jackknife_samples(values, samples)
    ! Obtain the jackknife samples for each propagator component
    ! samples stores the mean in the 0'th index of the final rank
    ! jackknife samples in the 1:nconfig index of the final rank
    real(DP), dimension(0:,0:,0:,0:,:,:,:), intent(in) :: values
    real(DP), dimension(0:,0:,0:,0:,:,:,0:), intent(out) :: samples

    ! Local vars
    real(DP) :: N
    integer :: nconfig, icfg

    nconfig = size(values, dim=7)
    write(*,*) 'nconfig = ', nconfig
    N = real(nconfig, DP)

    samples(:, :, :, :, :, :, 0) = sum(values, dim=7) / N

    do icfg = 1, nconfig
      samples(:, :, :, :, :, :, icfg) = (N * samples(:, :, :, :, :, :, 0) &
          - values(:, :, :, :, :, :, icfg)) &
          / (N - 1.0d0)
    end do

  end subroutine get_jackknife_samples

  subroutine analyse_ratios(samples, comb, momtype, ratios)
    ! Calculate the jackknife mean and error for each ratio
    ! Also calculate and store the exact ratio from the tensor structure
    real(DP), dimension(0:,0:,0:,0:,:,:,0:) :: samples
    integer, dimension(:, :) :: comb
    character(len= * ), intent(in) :: momtype
    real(DP), dimension(:, :), allocatable :: ratios
    ! Local vars
    real(DP), dimension(:), allocatable :: subsamples
    real(DP) :: mean, sub_mean, err
    real(DP) :: N
    integer :: n_combinations, nconfig
    integer :: ic, ix, iy, iz, it, counter
    integer :: num1, num2, denom1, denom2

    n_combinations = size(comb, dim=2)
    nconfig = size(samples, dim=7) - 1
    write(*,*) 'nconfig = ', nconfig
    allocate(subsamples(nconfig))
    if(allocated(ratios)) deallocate(ratios)
    allocate(ratios(3, n_combinations * nd ** 4))
    N = real(nconfig, DP)

    counter = 1
    do ic = 1, n_combinations
      do num1 = 1, nd
        do num2 = 1, nd
          do denom1 = 1, nd
            do denom2 = 1, nd
              ix = comb(1, ic)
              iy = comb(2, ic)
              iz = comb(3, ic)
              it = comb(4, ic)
              ! Calculate the global mean
              mean = samples(ix, iy, iz, it, num1, num2, 0) &
                  / samples(ix, iy, iz, it, denom1, denom2, 0)

              ! Calculate jackknife samples
              subsamples = samples(ix, iy, iz, it, num1, num2, 1:) &
                  / samples(ix, iy, iz, it, denom1, denom2, 1:)
              ! Calculate the jackknife mean and error
              sub_mean = sum(subsamples) / nconfig
              err = sqrt(((N - 1) / N) * sum((subsamples - mean)**2))

              ! Report the theory ratio, total mean and jackknife error
              ratios(1, counter) = exact_ratio(comb(:, ic), [num1, num2], &
                  [denom1, denom2], trim(momtype))
              ratios(2, counter) = mean
              ratios(3, counter) = err

              counter = counter + 1
            end do
          end do
        end do
      end do
    end do

  end subroutine analyse_ratios

end program GluonPropRatios
