!
!  A program to explore various momentum directions and lattice values
!  for Wilson and Improved gluon propagators.
!
program GluonPropAnalysis
  use kinds
  implicit none

  !     Defining constants (DO NOT CHANGE)
  integer, parameter :: PACTION=1, PHAT=0, WILSON=0, TREELW=1

  !     Lattice Parameters
  !
  ! integer :: nx, ny, nz, nt
  integer :: nsp, ntp, Ls, Lt, Lts
  !     nsp = spacial dimensions/2, Ls = spacial dimensions. Likewise for time


  real(DP)    :: angle, testAngle, mt, ms
  real(DP)    :: qx, qt
  real(DP)    :: sqr(4)
  real(DP)    :: length, dist, radius
  real(DP):: smallMom, smallGluAvg, smallGluUnc, smallx, smally, smallz, smallt
  integer :: np, ip, jp, kp, npsel, icon, ix, iy, iz, it
  integer :: maxMom

  !     Variables for averaging

  integer :: nregime, iregime
  real(DP)    :: lowerLim, upperLim
  real(DP)    :: beta
  integer :: innx, inny, innz, innt

  !     Variables for tree-level correction
  !     real apart, numer1, numer2, denom2
  !     real tree
  !     CHANGE HERE
  integer, parameter :: whichp=PACTION, action=TREELW

  !     File I/O
  integer, parameter :: infl = 101, cfgfl = 102
  integer :: nconfig
  character(len=256) :: CIN, COUT, CFG
  character(len=512) :: filename
  logical :: file_exists


  !-------------------------------------------- --------- --------- --

  !     Correlation functions

  real(dp), dimension(:,:), allocatable :: Gluon
  real(dp), dimension(:), allocatable :: GluonAvg, GluonUncert, Momentum, Momentum2
  ! real(dp) :: sum
  real(dp), dimension(:), allocatable :: x, y, z, t

  !     Record of selected momenta

  real(dp), dimension(:), allocatable :: selGlu, selUncert, selMom


  ! |--------------------------- Get input ----------------------------|

  write(*,*) "What is the text file containing the gluon propagator file names?"
  read(*,*) CIN
  write(*,*) trim(CIN)
  write(*,*)

  write(*,*) "What is the output directory?"
  read(*,*) COUT
  write(*,*) trim(COUT)
  write(*,*)

  write(*,*) 'Enter the maximum spatial lattice momentum component. nx/4 performs a half cut'
  read (*,*) maxMom
  write(*,*)

  write(*,*) "Enter the upper momentum limit for the cylinder cut."
  ! Some large number is good, don't need to cut here
  read (*,*) upperLim
  write(*,*)

  write(*,*)'Please enter the angle from the diagonal. 90 performs a cylinder cut'
  read (*,*) angle
  write(*,*)

  write(*,*)'Please enter the cylinder radius in spatial lattice units. 2 is standard'
  read (*,*) radius
  write(*,*)

  open(infl, file=trim(CIN), status="old", action="read")
  read(infl,*) nconfig

  ! Get lattice parameters from first input file
  read(infl,'(a)') CFG
  open(cfgfl, file=trim(CFG), status="old", action="read")
  read(cfgfl,*) beta
  read(cfgfl,*) nx, ny, nz, nt

  ! Set momentum constants:
  ! nsp = spacial dimensions/2, Ls = spacial length. Likewise for time
  nsp=nx/2
  ntp=nt/2
  Ls=nx
  Lt=nt
  Lts=2
  qx = 2.0 * pi / Ls
  qt = 2.0 * pi / Lt

  ! Allocate momentum variables
  ip = 0
  do it = 0, ntp
    do ix = 0, nsp
      do iy = 0, ix
        do iz = 0, iy
          ip = ip + 1
        end do
      end do
    end do
  end do

  write(*,*)'The number of momenta np is ',ip
  np = ip

  allocate(Gluon(np,nconfig))
  allocate(GluonAvg(np))
  allocate(GluonUncert(np))
  allocate(Momentum(np))
  allocate(Momentum2(np))
  allocate(x(np))
  allocate(y(np))
  allocate(z(np))
  allocate(t(np))
  allocate(selGlu(np))
  allocate(selUncert(np))
  allocate(selMom(np))

  ip = 0
  do it = 0, ntp
    do ix = 0, nsp
      do iy = 0, ix
        do iz = 0, iy
          ip = ip + 1
          x(ip) = ix
          y(ip) = iy
          z(ip) = iz
          t(ip) = it
        end do
      end do
    end do
  end do

  ! Read in gluon propagator data
  do icon = 1, nconfig

    if(icon > 1) then
      read(infl,'(a)') CFG
      open(cfgfl, file=trim(CFG), status="old", action="read")
      read(cfgfl,*) beta
      read(cfgfl,*) nx, ny, nz, nt
    end if

    do ip = 1, np
      read (cfgfl,*) Gluon(ip,icon)
      if(isnan(Gluon(ip,icon))) then
        write(*,*) "NaN in cfg ", trim(CFG), ", line ", ip
      end if
    end do

    close(cfgfl)
  end do

  close(infl)

  write(*,*) "Finished reading"

  ! |--------------- Calculate average propagator value ---------------|

  do ip = 1, np
    call jackknife(Gluon(ip, :), GluonAvg(ip), GluonUncert(ip))
    ! sum = 0.0d0
    ! do icon=1,nconfig
    !    sum = sum + Gluon(ip,icon)
    ! end do
    ! GluonAvg(ip) = sum / nconfig

    ! sum = 0.0d0
    ! do icon=1,nconfig
    !    sum = sum + (GluonAvg(ip) - Gluon(ip,icon))**2
    ! end do
    ! GluonUncert(ip) = sqrt(sum / (nconfig - 1) / nconfig)
  enddo


  ! |------------------- Calculate lattice momenta --------------------|

  if( whichp == PHAT ) then
    !     Continuum Momenta

    do ip = 1, np
      Momentum(ip) = sqrt(qx**2 * (x(ip)**2 + y(ip)**2 + z(ip)**2) &
          + qt**2 * t(ip)**2 )
    end do

  elseif( whichp == PACTION ) then

    if( action == WILSON ) then
      !     This time with 2 * sin(p/2)  ie.  Lattice Momenta

      do ip = 1, np
        Momentum(ip) = 2.0 * sqrt( &
            (sin(qx * x(ip) / 2) )**2 + &
            (sin(qx * y(ip) / 2) )**2 + &
            (sin(qx * z(ip) / 2) )**2 + &
            (sin(qt * t(ip) / 2) )**2  )
      end do

    elseif( action == TREELW ) then
      !     Momenta from tree-level Luesher Weisz action
      !     2 * sqrt( sin^2(p/2) + (1/3)sin^4(p/2)

      do ip = 1, np
        Momentum(ip) = 2.0 * sqrt( &
            ( sin(qx * x(ip) / 2) )**2 + &
            ( sin(qx * y(ip) / 2) )**2 + &
            ( sin(qx * z(ip) / 2) )**2 + &
            ( sin(qt * t(ip) / 2) )**2   &
            + ( ( sin(qx * x(ip) / 2) )**4 + &
            ( sin(qx * y(ip) / 2) )**4 + &
            ( sin(qx * z(ip) / 2) )**4 + &
            ( sin(qt * t(ip) / 2) )**4 ) / 3.0)
      end do
    endif
  endif

  ! |-------------------- Sort output by momentum ---------------------|

  do ip = 1, np-1
    smallMom = Momentum(ip)
    do jp =ip+1, np-1
      if (smallMom > Momentum(jp)) then
        smallMom = Momentum(jp)
        smallGluAvg = GluonAvg(jp)
        smallGluUnc = GluonUncert(jp)
        smallx = x(jp)
        smally = y(jp)
        smallz = z(jp)
        smallt = t(jp)

        do kp = jp-1, ip, -1
          Momentum(kp+1) = Momentum(kp)
          GluonAvg(kp+1) = GluonAvg(kp)
          GluonUncert(kp+1) = GluonUncert(kp)
          x(kp+1) = x(kp)
          y(kp+1) = y(kp)
          z(kp+1) = z(kp)
          t(kp+1) = t(kp)
        end do

        Momentum(ip) = smallMom
        GluonAvg(ip) = smallGluAvg
        GluonUncert(ip) = smallGluUnc
        x(ip) = smallx
        y(ip) = smally
        z(ip) = smallz
        t(ip) = smallt
      end if
    end do
  end do

  ! |----------------------- Open output files ------------------------|

  !     input correlation functions for all configurations and accumulate.

  OPEN(13,file=trim(COUT)//'diagonal'//'.dat', status ='replace')
  OPEN(14,file=trim(COUT)//'cartesian'//'.dat', status ='replace')
  OPEN(15,file=trim(COUT)//'time'//'.dat', status = 'replace')
  OPEN(16,file=trim(COUT)//'other'//'.dat', status ='replace')

  !     Select diagonal & cartesian

  do ip = 1, np

    if( x(ip) .le. maxMom .and. y(ip) .le. maxMom .and. &
        z(ip) .le. maxMom .and. t(ip) .le. maxMom*Lts ) then

      if( y(ip) .eq. 0.0 .and. z(ip) .eq. 0.0 .and. t(ip) .eq. 0.0 ) then

        write(14,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)

      else if( x(ip) .eq. 0.0 .and. y(ip) .eq. 0.0 .and. z(ip) .eq. 0.0 ) then

        write(15,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)

      else if( x(ip) .eq. y(ip) .and. y(ip) .eq. z(ip) .and. Lts * z(ip) .eq. t(ip) ) then

        write(13,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)

      else

        write(16,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)
      end if
    end if
  end do

  close (13)
  close (14)
  close (15)
  close (16)


  !-----------------------------------------------------------------------

  ! This time for all points within some angle from the diagonal
  ! also with cylinder option

  OPEN(17,file=trim(COUT)//'selected'//'.dat',status='replace')
  OPEN(18,file=trim(COUT)//'discarded'//'.dat',status='replace')

  npsel = 0
  selGlu = 0.0d0
  selMom = 0.0d0
  selUncert = 0.0d0
  lowerLim = 0.0d0

  ! Convert to radians
  angle = angle * pi / 180.0


  do ip = 1, np

    ! Check only if momentum is in window of opportunity
    ! Time direction is 2 times the spatial
    if (x(ip) .le. maxMom .and. y(ip) .le. maxMom .and. z(ip) .le. maxMom .and. t(ip) .le. maxMom*Lts ) then

      if (Momentum(ip).ge.lowerLim .and. Momentum(ip).lt.upperLim) then

        length = sqrt( x(ip)**2 + y(ip)**2 + z(ip)**2 + (t(ip)**2)/Lts**2 )
        if (length .ne. 0.0) then
          testAngle = acos((x(ip) + y(ip) + z(ip) + t(ip)/Lts) / (sqrt(4.0) * length) )
          if (testAngle.le.angle) then

            !     Check cylinder

            dist = length * sin(testAngle)
            if (dist .le. radius) then
              write(17,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)
              !    Make arrays of only the selected data
              npsel = npsel+1
              selMom(npsel) = Momentum(ip)
              selGlu(npsel) = GluonAvg(ip)
              selUncert(npsel) = GluonUncert(ip)
            else
              write(18,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)
            endif

          else
            write(18,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)
          endif

        else
          write(18,*) Momentum(ip), GluonAvg(ip), GluonUncert(ip)
        end if

      end if
    end if
  end do


  close (17)
  close (18)

  write(*,*) "Finished"

contains

  subroutine jackknife(array, mean, sigma)
    real(DP), dimension(:) :: array
    real(DP) :: mean, sigma

    ! Local vars
    real(DP), dimension(:), allocatable :: jack_samples
    real(DP) :: N

    N = real(size(array), DP)
    allocate(jack_samples(size(array)))

    mean = sum(array) / N

    jack_samples = (N * mean - array) / (N - 1.0d0)

    sigma = sqrt(((N - 1) / N) * sum((jack_samples - mean)**2))

  end subroutine jackknife

end program GluonPropAnalysis
