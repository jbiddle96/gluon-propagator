! Program to calculate the gluon propagator in coordinate space.
! By Patrick Bowman
! 03/06/1999
! Updated on 19/06/2003

! Input Unit 11
! (gauge fixed) Gauge configurations in short SU(3)


program GLUON_PROPAGATORS
  use Kinds
  use FileIO
  use MatrixOps
  use Gaugefield
  use iso_c_binding
  use GluonOps
  implicit none

  ! Link variables split into real and imaginary parts
  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: UR, UI

  ! Gluon fields
  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: GLUR, GLUI
  double complex, dimension(:,:,:,:,:,:,:), allocatable :: momglu
  double complex, dimension(:,:,:,:), allocatable :: gluc


  ! Propagator
  real(DP), dimension(:,:,:,:), allocatable :: DM, MPROP
  double complex, dimension(:,:,:,:), allocatable :: mprop_cmplx
  real(DP), dimension(:,:,:,:,:), allocatable :: TRACE

  ! Temporal propagator
  real(DP), dimension(:), allocatable :: Dp4

  ! Coordinates
  integer :: IC, JC, KC        ! 1, NC
  integer :: ID, IMU, INU      ! 1, ND
  integer :: IX, IY, IZ, IT    ! 1, NX etc.
  integer :: JX, JY, JZ
  integer :: i                 ! Dummy loop variable

  ! Variables for ratio calculation
  integer :: maxratiomom, n_combinations, counter
  integer, dimension(:, :), allocatable :: comb

  ! Constant - twice the trace
  real(DP) :: COEFF

  ! File I/O
  real(DP) :: BETA, MEANLINK
  character(len=256) :: CIN
  character(len=256) :: GLU_OUT, TGLU_OUT
  character(len=64)  :: CFG
  character(len=10)  :: type
  character(len=4)   :: the_format

  integer :: PX, PY, PZ, PT


  integer(c_long) :: fplan
  integer(c_int), dimension(4) :: dims
  integer(c_int) :: dir
  integer :: npx, npy, npz, npt



  ! **************************************************
  ! Ask for input
  ! **************************************************

  write(*,*) "What is the input gauge field?"
  read(*,*) CIN
  write(*,*) trim(CIN)

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt

  write(*, *) "What is the name of the gluon propagator output file?"
  read(*,*) GLU_OUT
  write(*,*) trim(GLU_OUT)

  write(*, *) "What is the name of the temporal propagator output file?"
  read(*,*) TGLU_OUT
  write(*,*) trim(TGLU_OUT)

  write(*,*) "What is the file format?"
  read(*,*) the_format

  call flushout()

  V = nx * ny * nz * nt
  ldims = (/nx, ny, nz, nt/)
  ! |------------------------ Allocate arrays -------------------------|

  allocate(UR(NX,NY,NZ,NT,ND,NC,NC), UI(NX,NY,NZ,NT,ND,NC,NC))
  allocate(GLUR(NX,NY,NZ,NT,ND,NC,NC), GLUI(NX,NY,NZ,NT,ND,NC,NC))
  allocate(gluc(0:nx-1,0:ny-1,0:nz-1,0:nt-1))
  allocate(momglu(0:nx-1,0:ny-1,0:nz-1,0:nt-1,nd,nc,nc))
  allocate(DM(0:NX/2,0:NY/2,0:NZ/2,0:NT/2), MPROP(0:NX - 1,0:NY - 1,0:NZ - 1,0:NT - 1))
  allocate(mprop_cmplx(0:NX - 1,0:NY - 1,0:NZ - 1,0:NT - 1))
  allocate(Dp4(NT))
  allocate(TRACE(NX,NY,NZ,NT,ND))

  COEFF = 2.0d0 / real(V * (NC**2 - 1), DP)

  ! **************************************************
  ! Read in Gauge Configuration
  ! **************************************************

  if(the_format=="cssm") then
    ! For double precision configurations in compact form
    call ReadGaugeField_cssm(trim(CIN), ur, ui, meanlink, beta)
  else if(the_format=="ildg") then
    call ReadGaugeField_ildg(trim(CIN), ur, ui, meanlink, beta)
  else
    write(*,*) "Unrecognised format, aborting"
    stop
  end if

  ! **************************************************
  ! Calculate the gluon field
  ! Note: Usual definition of A_\mu
  ! **************************************************

  ! Calculate real and imaginary parts of A(L-mu) from the gauge links
  ! UR = real parts, UI = imaginary parts
  do IC = 1, NC
    do JC = 1, NC
      GLUR(:,:,:,:,:,IC,JC) = UR(:,:,:,:,:,IC,JC) - UR(:,:,:,:,:,JC,IC)
    enddo
  enddo
  UR = GLUR
  do IC = 1, NC
    do JC = 1, NC
      GLUR(:,:,:,:,:,IC,JC) = UI(:,:,:,:,:,IC,JC) + UI(:,:,:,:,:,JC,IC)
      ! Note addition not subtraction because of conjugate-transpose
    enddo
  enddo
  UI = GLUR

  ! Calculate trace. Only need imaginary parts as real parts are removed by conjugate-transpose
  TRACE = 0.d0
  do IC = 1, NC
    TRACE = TRACE + GLUR(:,:,:,:,:,IC,IC)
  enddo

  ! Divide trace by 3
  TRACE = TRACE / dble(NC)

  ! Subtract trace from imaginary part
  do IC = 1, NC
    UI(:,:,:,:,:,IC,IC) = GLUR(:,:,:,:,:,IC,IC) - TRACE
  enddo

  ! -i / 2u_0
  GLUR = UI / (2.d0 * MEANLINK)
  UI = -UR / (2.d0 * MEANLINK)
  UR = GLUR

  call flushout()

  ! **************************************************
  ! Calculate the gluon field in momentum space
  ! **************************************************

  dims(1) = nx
  dims(2) = ny
  dims(3) = nz
  dims(4) = nt
  dir = +1

  call fuse_plan_4d(fplan, dims, gluc, dir)
  write(*,*) 'Done Plan'


  ! Convert to complex type
  do id = 1, nd
    do ic = 1, nc
      do jc = 1, nc
        forall(ix = 1:nx, iy = 1:ny, iz = 1:nz, it = 1:nt )
          gluc(ix-1,iy-1,iz-1,it-1) = cmplx(ur(ix,iy,iz,it,id,ic,jc), &
              ui(ix,iy,iz,it,id,ic,jc))
        end forall
        ! Perform fftw, store coefficients in gluc
        call fuse_sgfix(fplan)
        momglu(:,:,:,:,id,ic,jc) = gluc(:,:,:,:)
      enddo
    enddo
  enddo
  call fuse_destroyplan(fplan)

  write(*,*) "Performed DFT"
  ! Phases from midpoint definition cancel in scalar part


  ! **************************************************
  ! Now calculate the (scalar) propagator
  ! **************************************************

  ! Calculate expectation value
  mprop_cmplx = 0.d0
  do PX = 0, NX - 1
    if( px == 0 ) then
      npx = 0
    else
      npx = nx - px
    endif
    do PY = 0, NY - 1
      if( py == 0 ) then
        npy = 0
      else
        npy = ny - py
      endif
      do PZ = 0, NZ - 1
        if( pz == 0 ) then
          npz = 0
        else
          npz = nz - pz
        endif
        do PT = 0, NT - 1
          if( pt == 0 ) then
            npt = 0
          else
            npt = nt - pt
          endif
          do ID = 1, ND
            mprop_cmplx(px,py,pz,pt) = mprop_cmplx(px,py,pz,pt) + &
                Tr(matmul(momglu(px,py,pz,pt,ID, :, :), &
                momglu(npx,npy,npz,npt,ID,:,:)))
          enddo
        enddo
      enddo
    enddo
  enddo

  mprop_cmplx = COEFF * mprop_cmplx
  mprop=real(mprop_cmplx, DP)

  write(*,*) "Calculated expectation value"

  ! **************************************************
  !  Finally, do Z_3 averaging.
  ! **************************************************

  DM = 0.d0
  do PX = 0, NX/2
    do PY = 0, PX
      do PZ = 0, PY
        do PT = 0, NT/2
          DM(PX,PY,PZ,PT) = (MPROP(PX,PY,PZ,PT) &
              + MPROP(PX,PZ,PY,PT) + MPROP(PZ,PX,PY,PT) &
              + MPROP(PZ,PY,PX,PT) + MPROP(PY,PZ,PX,PT) &
              + MPROP(PY,PX,PZ,PT)) / 6.d0
        enddo
      enddo
    enddo
  enddo

  DM = DM / dble(ND - 1)

  ! Construct temporal propagator
  Dp4 = [DM(0, 0, 0, :), DM(0, 0, 0, (NT / 2 - 1):1:-1)]

  write(*,*) "Finished Z3 averaging"

  open(22,file=trim(GLU_OUT),form='formatted',status='replace', action='write')
  write(22,1) BETA
  write(22,'(I3)') NX, NY, NZ, NT
  do IT = 0, NT/2
    do IX = 0, NX/2
      do IY = 0, IX
        do IZ = 0, IY
          write(22,*) DM(IX,IY,IZ,IT)
          !write(22,'(f12.8)') DM(IX,IY,IZ,IT)
        enddo
      enddo
    enddo
  enddo
  write(22,1) MEANLINK
  close(22)

  write(*,*) "Finished writing gluon propagator"


  open(22,file=trim(TGLU_OUT),form='formatted',status='replace', action='write')
  write(22,1) BETA
  write(22,'(I3)') NX, NY, NZ, NT
  do IT = 1, NT
    write(22,*) Dp4(IT)
  enddo
  write(22,1) MEANLINK
  close(22)

  write(*,*) "Finished writing temporal propagator"

1 format(ES14.6)
2 format(I3,I3,I3,I3,ES12.4)

contains

  subroutine dft(in, out)
    complex(DP), dimension(:) :: in, out

    integer :: i, j, N, N_out

    N = size(in)
    N_out = size(out)
    if(N .NE. N_out) then
      write(*,*) "Size mismatch between input and output arrays, exiting"
      stop
    end if
    out = 0.0d0
    do i = 0, N - 1

      do j = 0, N - 1
        out(i + 1) = out(i + 1) + in(j + 1) * exp(-(i1 * 2.0d0 * pi / real(N, DP)) * real(i * j, DP))
      end do
    end do

  end subroutine dft

end program GLUON_PROPAGATORS
