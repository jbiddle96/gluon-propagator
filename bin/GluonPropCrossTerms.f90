! Program to calculate gluon propagator cross terms in momentum space.
! Original code by Patrick Bowman
! 03/06/1999
! Updated on 19/06/2003
! Updated 12/08/2020 by James Biddle


program GluonPropCrossTerms
  use Kinds
  use FileIO
  use iso_c_binding
  use MatrixOps
  use Gaugefield
  use GluonOps
  implicit none

  ! Link variables split into real and imaginary parts
  real(dp), dimension(:,:,:,:,:,:,:), allocatable :: UR, UI
  complex(dc), dimension(:,:,:,:,:,:,:), allocatable :: U_VO, U_VR, A_VO, A_VR, UmUdag

  ! Gluon fields
  !  real(dp), dimension(NX,NY,NZ,NT,ND,NC,NC) :: GLUR, GLUI
  complex(dc), dimension(:,:,:,:), allocatable :: gluc
  !  real(dp), dimension(0:nx-1,0:ny-1,0:nz-1,0:nt-1,nd,nc,nc) :: momglur, momglui


  ! Propagator
  real(dp), dimension(:,:,:,:), allocatable :: DM, MPROP

  real(dp) :: DUMMY

  real(dp), dimension(:,:,:,:,:), allocatable :: TRACE

  ! Coordinates
  integer :: IC, JC, KC        ! 1, NC
  integer :: ID, IMU, INU      ! 1, ND
  integer :: IX, IY, IZ, IT    ! 1, NX etc.
  integer :: JX, JY, JZ
  !integer :: i                 ! Dummy loop variable

  ! Constant - twice the trace
  real(dp) :: COEFF

  ! File I/O
  real(dp) :: BetaVO, BetaVR,LastPLAQ, PlaqBarAVG, MeanlinkVO, MeanlinkVR
  character(len=256) :: CINVO, CINVR
  character(len=256) :: COUT
  character(len=64)  :: CFG
  character(len=10)  :: type
  character(len=4)   :: the_format

  integer :: PX, PY, PZ, PT


  integer(c_long) :: fplan
  integer(c_int), dimension(4) :: dims
  integer(c_int) :: dir
  integer :: npx, npy, npz, npt

  ! **************************************************
  ! Ask for input
  ! **************************************************

  write(*, '("What is the Vortex Only input directory?")')
  read(*,*) CINVO
  write(*,*) trim(CINVO)

  write(*, '("What is the Vortex Removed input directory?")')
  read(*,*) CINVR
  write(*,*) trim(CINVR)

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt

  write(*, '("what is the output directory?")')
  read(*,*) COUT
  write(*,*) trim(COUT)

  write(*,*) "What is the file format?"
  read(*,*) the_format

  call flushout()

  V = nx * ny * nz * nt
  ldims = (/nx, ny, nz, nt/)

  ! |------------------------ Allocate arrays -------------------------|

  allocate(UR(NX,NY,NZ,NT,ND,NC,NC), UI(NX,NY,NZ,NT,ND,NC,NC))
  allocate(U_VO(0:nx-1,0:ny-1,0:nz-1,0:nt-1,ND,NC,NC), U_VR(0:nx-1,0:ny-1,0:nz-1,0:nt-1,ND,NC,NC))
  allocate(A_VO(0:nx-1,0:ny-1,0:nz-1,0:nt-1,ND,NC,NC), A_VR(0:nx-1,0:ny-1,0:nz-1,0:nt-1,ND,NC,NC))
  allocate(UmUdag(0:nx-1,0:ny-1,0:nz-1,0:nt-1,ND,NC,NC))
  ! allocate(GLUR(NX,NY,NZ,NT,ND,NC,NC), GLUI(NX,NY,NZ,NT,ND,NC,NC))
  allocate(gluc(0:nx-1,0:ny-1,0:nz-1,0:nt-1))
  ! allocate(momglur(0:nx-1,0:ny-1,0:nz-1,0:nt-1,nd,nc,nc))
  ! allocate(momglui(0:nx-1,0:ny-1,0:nz-1,0:nt-1,nd,nc,nc))
  allocate(DM(0:NX/2,0:NY/2,0:NZ/2,0:NT/2), MPROP(0:NX/2,0:NY/2,0:NZ/2,0:NT/2))
  allocate(TRACE(NX,NY,NZ,NT,ND))

  COEFF = 2.d0 / dble(NX * NY * NZ * NT * (NC**2 - 1))


  ! **************************************************
  ! Read in Gauge Configuration
  ! **************************************************

  if(the_format=="cssm") then
    call ReadGaugeField_cssm(trim(CINVO), ur, ui, meanlinkVO, betaVO)
    U_VO = cmplx(UR, UI)

    call ReadGaugeField_cssm(trim(CINVR), ur, ui, meanlinkVR, betaVR)
    U_VR = cmplx(UR, UI)

  else if(the_format=="ildg") then
    call ReadGaugeField_ildg(trim(CINVO), ur, ui, meanlinkVO, betaVO)
    U_VO = cmplx(UR, UI)

    call ReadGaugeField_ildg(trim(CINVR), ur, ui, meanlinkVR, betaVR)
    U_VR = cmplx(UR, UI)

  else
    write(*,*) "Unrecognised format, aborting"
    stop
  end if
  write(*,*) "VO meanlink = ", MEANLINKVO
  write(*,*) "VR meanlink = ", MEANLINKVR

  ! **************************************************
  ! Calculate the gluon field
  ! Note: Usual definition of A_\mu
  ! **************************************************


  forall(ix=0:nx-1, iy=0:ny-1, iz=0:nz-1, it=0:nt-1, id=1:nd)
    UmUdag(ix,iy,iz,it,id,:,:) = U_VO(ix,iy,iz,it,id,:,:) - ConjTrans(U_VO(ix,iy,iz,it,id,:,:))
    A_VO(ix,iy,iz,it,id,:,:) = -i1/meanlinkVO * (UmUdag(ix,iy,iz,it,id,:,:)/2.0d0 &
        - Tr(UmUdag(ix,iy,iz,it,id,:,:)) * ID3x3/6.0d0 )

    UmUdag(ix,iy,iz,it,id,:,:) = U_VR(ix,iy,iz,it,id,:,:) - ConjTrans(U_VR(ix,iy,iz,it,id,:,:))
    A_VR(ix,iy,iz,it,id,:,:) = -i1/meanlinkVR * (UmUdag(ix,iy,iz,it,id,:,:)/2.0d0 &
        - Tr(UmUdag(ix,iy,iz,it,id,:,:)) * ID3x3/6.0d0 )
  end forall

  ! Calculate real and imaginary parts of A(L-mu) from the gauge links
  ! UR = real parts, UI = imaginary parts
  ! do IC = 1, NC
  !    do JC = 1, NC
  !       GLUR(:,:,:,:,:,IC,JC) = UR(:,:,:,:,:,IC,JC) - UR(:,:,:,:,:,JC,IC)
  !    enddo
  ! enddo
  ! UR = GLUR
  ! do IC = 1, NC
  !    do JC = 1, NC
  !       GLUR(:,:,:,:,:,IC,JC) = UI(:,:,:,:,:,IC,JC) + UI(:,:,:,:,:,JC,IC)
  !       ! Note addition not subtraction because of conjugate-transpose
  !    enddo
  ! enddo
  ! UI = GLUR
  !
  ! ! Calculate trace. Only need imaginary parts as real parts are removed by conjugate-transpose
  ! TRACE = 0.d0
  ! do IC = 1, NC
  !    TRACE = TRACE + GLUR(:,:,:,:,:,IC,IC)
  ! enddo
  !
  ! ! Divide trace by 3
  ! TRACE = TRACE / NC
  !
  ! ! Subtract trace from imaginary part
  ! do IC = 1, NC
  !    UI(:,:,:,:,:,IC,IC) = GLUR(:,:,:,:,:,IC,IC) - TRACE
  ! enddo
  !
  ! ! -i / 2u_0
  ! GLUR = UI / (2.d0 * MEANLINK)
  ! UI = -UR / (2.d0 * MEANLINK)
  ! UR = GLUR

  call flushout()

  ! **************************************************
  ! Calculate the gluon field in momentum space
  ! **************************************************

  dims(1) = nx
  dims(2) = ny
  dims(3) = nz
  dims(4) = nt
  dir = +1

  call fuse_plan_4d(fplan, dims, gluc, dir)
  write(*,*) 'Done Plan'

  do id = 1, nd
    do ic = 1, nc
      do jc = 1, nc
        ! Vortex Only fft
        forall(ix = 0:nx-1, iy = 0:ny-1, iz = 0:nz-1, it = 0:nt-1 )
          !gluc(ix-1,iy-1,iz-1,it-1) = cmplx(ur(ix,iy,iz,it,id,ic,jc), &
          !     ui(ix,iy,iz,it,id,ic,jc))
          gluc(ix,iy,iz,it) = A_VO(ix,iy,iz,it,id,ic,jc)
        end forall
        ! Perform fftw, store coefficients in gluc
        call fuse_sgfix(fplan)
        A_VO(:,:,:,:,id,ic,jc) = gluc(:,:,:,:)
      enddo
    enddo
  enddo
  call fuse_destroyplan(fplan)

  gluc=0.0d0
  call fuse_plan_4d(fplan, dims, gluc, dir)
  write(*,*) 'Done Plan'

  do id = 1, nd
    do ic = 1, nc
      do jc = 1, nc
        ! Vortex Removed fft
        forall(ix = 0:nx-1, iy = 0:ny-1, iz = 0:nz-1, it = 0:nt-1 )
          !gluc(ix-1,iy-1,iz-1,it-1) = cmplx(ur(ix,iy,iz,it,id,ic,jc), &
          !     ui(ix,iy,iz,it,id,ic,jc))
          gluc(ix,iy,iz,it) = A_VR(ix,iy,iz,it,id,ic,jc)
        end forall
        ! Perform fftw, store coefficients in gluc
        call fuse_sgfix(fplan)
        A_VR(:,:,:,:,id,ic,jc) = gluc(:,:,:,:)

      enddo
    enddo
  enddo
  call fuse_destroyplan(fplan)

  ! Phases from midpoint definition cancel in scalar part


  ! **************************************************
  ! Now calculate the (scalar) propagator
  ! **************************************************

  ! Calculate expectation value


  MPROP = 0.d0
  do PX = 0, NX/2
    if( px == 0 ) then
      npx = 0
    else
      npx = nx - px
    endif
    do PY = 0, NY/2
      if( py == 0 ) then
        npy = 0
      else
        npy = ny - py
      endif
      do PZ = 0, NZ/2
        if( pz == 0 ) then
          npz = 0
        else
          npz = nz - pz
        endif
        do PT = 0, NT/2
          if( pt == 0 ) then
            npt = 0
          else
            npt = nt - pt
          endif
          DUMMY = 0.d0
          do ID = 1, ND
            !do IC = 1, NC
            !   do KC = 1, NC
            DUMMY = DUMMY &
                + real(Tr(matmul(A_VR(px,py,pz,pt,ID,:,:), &
                A_VO(npx,npy,npz,npt,ID,:,:))),dp) &
                + real(Tr(matmul(A_VO(px,py,pz,pt,ID,:,:), &
                A_VR(npx,npy,npz,npt,ID,:,:))),dp)
            !(momglur(px,py,pz,pt,ID,IC,KC) &
            !* momglur(npx,npy,npz,npt,ID,KC,IC)) &
            !- (momglui(px,py,pz,pt,ID,IC,KC) &
            !* momglui(npx,npy,npz,npt,ID,KC,IC))
            !  enddo
            !enddo
          enddo
          mprop(px,py,pz,pt) = DUMMY
        enddo
      enddo
    enddo
  enddo

  mprop = COEFF * mprop

  ! **************************************************
  !  Finally, do Z_3 averaging.
  ! **************************************************

  DM = 0.d0
  do PX = 0, NX/2
    do PY = 0, PX
      do PZ = 0, PY
        do PT = 0, NT/2
          DM(PX,PY,PZ,PT) = (MPROP(PX,PY,PZ,PT) &
              + MPROP(PX,PZ,PY,PT) + MPROP(PZ,PX,PY,PT) &
              + MPROP(PZ,PY,PX,PT) + MPROP(PY,PZ,PX,PT) &
              + MPROP(PY,PX,PZ,PT)) / 6.d0
        enddo
      enddo
    enddo
  enddo

  DM = DM / real(ND - 1, dp)

  open(22,file=trim(COUT),form='formatted',status='replace', action='write')
  write(22,1) BETAVO
  write(22,'(I3)') NX, NY, NZ, NT
  do IT = 0, NT/2
    do IX = 0, NX/2
      do IY = 0, IX
        do IZ = 0, IY
          write(22,*) DM(IX,IY,IZ,IT)
        enddo
      enddo
    enddo
  enddo
  write(22,1) MEANLINKVO
  write(22,1) MEANLINKVR
  close(22)

1 format(ES14.6)
2 format(I3,I3,I3,I3,ES12.4)

end program GluonPropCrossTerms
