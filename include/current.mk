F90C = ifort
F90FLAGS = -cpp $(OPTS)
OPTS = -O3 -ip -assume byterecl -convert big_endian -shared-intel -mcmodel=medium -mkl=sequential
# OPTS = -O0 -traceback -check bounds -ip -xHost -assume byterecl -convert big_endian -mcmodel=medium -mkl=sequential -shared-intel

CC = icc
CFLAGS = -O3 -ip -xHost -mcmodel=medium -shared-intel
COBJS = $(patsubst ../src/%.c,../src/%.o,$(wildcard ../src/*.c))

# LINK = -L../lib/ -lcola # -lMCG #$(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_sequential.a -Wl,--end-group $(MKLROOT)/lib/intel64/libmkl_blacs_openmpi_lp64.a -lpthread -lm

# INCLUDE = -I../src/ -I$(MKLROOT)/include/intel64/lp64 -I$(MKLROOT)/include # -I../colasrc/
INCLUDE = -I../src/ -I/apps/fftw3/3.3.8/include/

%.o: %.f90
	$(F90C) $(F90FLAGS) -c $(INCLUDE) $<

%.o: %.c
	$(CC) $(CFLAGS) -c $(INCLUDE) $<

%.x: %.f90
	$(F90C) $(F90FLAGS) -o $@ $(INCLUDE) $(COBJS) $^
