/* Wrapper for FFTW complex fft in N dimensions */
/* For use with the general planner */
/* FT in place and NOT normalised for the inverse */

#include <fftw3.h>

#define NDIMS 4

void fuse_sgfix_(fftw_plan *pf)
{
  fftw_execute(*pf);

  return;
}


void fuse_plan_4d_(fftw_plan *pf, int *dims, fftw_complex *in, int *dir)
{
  int nd = NDIMS;
  int id;
  int revdims[NDIMS];


  revdims[0] = dims[3];
  revdims[1] = dims[2];
  revdims[2] = dims[1];
  revdims[3] = dims[0];

  if( *dir > 0 ) {
    // printf("Get FFT plan.\n");
    *pf = fftw_plan_dft(nd, revdims, in, in, FFTW_FORWARD, FFTW_MEASURE);
  } else {
    // printf("Get inverse FFT plan.\n");
    *pf = fftw_plan_dft(nd, revdims, in, in, FFTW_BACKWARD, FFTW_MEASURE);
  }

  return;
}

void fuse_plan_1d_(fftw_plan *pf, int *dim, fftw_complex *in, int *dir)
{
  int id;

  if( *dir > 0 ) {
    // printf("Get FFT plan.\n");
    *pf = fftw_plan_dft_1d(*dim, in, in, FFTW_FORWARD, FFTW_MEASURE);
  } else {
    // printf("Get inverse FFT plan.\n");
    *pf = fftw_plan_dft_1d(*dim, in, in, FFTW_BACKWARD, FFTW_MEASURE);
  }

  return;
}

void fuse_destroyplan_(fftw_plan *pf)
{
  fftw_destroy_plan(*pf);
  fftw_cleanup();
  return;
}
