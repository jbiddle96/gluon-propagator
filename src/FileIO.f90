module FileIO
  use Kinds
  use Gaugefield
  use MatrixOps
  implicit none

contains

  subroutine ReadGaugeField_cssm(filename,ur,ui,uzero,beta)
    !
    !  cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    !  this subroutine reads in the link variables.
    !
    !  Author: Derek B. Leinweber
    !
    implicit none

    !     global variables

    character(len=*)                                        :: filename
    double precision,dimension(nx,ny,nz,nt,nd,nc,nc)        :: ur,ui
    double precision                                        :: beta,uzero
    !character(len=*),intent(in)                                  :: filename
    !double precision,dimension(nx,ny,nz,nt,nd,nc,nc),intent(inout) :: ur,ui
    !double precision,intent(out)                                 :: beta,uzero
!HPF$ DISTRIBUTE ur(*,*,BLOCK,*,*,*,*)
!HPF$ DISTRIBUTE ui(*,*,BLOCK,*,*,*,*)


    !     Local variables
    integer                                                 :: nfig,nxf,nyf,nzf,ntf
    double precision                                        :: lastPlaq,plaqbarAvg
    integer                                                 :: ic

    !     Execution begins
    open(101,file=filename,form='unformatted',status='old',action='read')

    read (101) nfig,beta,nxf,nyf,nzf,ntf
    !write(*,*) nfig,beta,nxf,nyf,nzf,ntf

    do ic = 1, nc-1
       read (101) ur(:,:,:,:,:,ic,:)
       read (101) ui(:,:,:,:,:,ic,:)
    end do

     ! Orthonormalise input
     call MAKESU3(UR, UI)

    read (101) lastPlaq, plaqbarAvg, uzero
    close(101)

    return

  end subroutine ReadGaugeField_cssm

  subroutine ReadGaugeField_ildg(filename,ur,ui,uzero,beta)
    implicit none

    character(len=*)                         :: filename
    real(DP),dimension(nx,ny,nz,nt,nd,nc,nc) :: ur,ui
    real(DP)                                 :: uzero,beta

    ! Local vars
    complex(dc), dimension(:,:,:,:,:,:,:), allocatable  :: U_lxd
    integer :: matrix_len, irecl, irec
    integer :: ic, jc, mu

    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*nx*ny*nz*nt
    allocate(U_lxd(nc,nc,nd,nx,ny,nz,nt))

    open(101,file=filename,form='unformatted',access='direct',status='old',action='read',recl=irecl)
    irec = 1
    read(101,rec=irec) U_lxd
    close(101)

    ! Take transpose and rearrange indices
    do mu = 1,nd
      do jc = 1,nc; do ic = 1,nc
        ur(:,:,:,:,mu,ic,jc) = real(U_lxd(jc,ic,mu,:,:,:,:))
        ui(:,:,:,:,mu,ic,jc) = aimag(U_lxd(jc,ic,mu,:,:,:,:))
      end do; end do
    end do

    ! Orthonormalise input
    call MAKESU3(UR, UI)

    ! u0 is not stored, so calculate it
    call Getuzero(ur, ui, uzero)

    ! Beta is also not stored, set it to one
    beta = 1.0d0

    deallocate(U_lxd)

  end subroutine ReadGaugeField_ildg

  subroutine write_ratios(unit, dir, ratios, comb)
    integer, intent(in) :: unit
    character(len= * ), intent(in) :: dir
    real(DP), dimension(:, :), intent(in) :: ratios
    integer, dimension(:, :) :: comb

    ! Local vars
    integer :: n_combinations
    integer :: i, num1, num2, denom1, denom2, counter
    character(len=64) :: filename

    n_combinations = size(comb, dim=2)

    counter = 1
    do i = 1, n_combinations
      write(filename, '(2a,4i0,a)')trim(dir), "gluprop_ratio_q", comb(:, i), ".dat"
      open(unit, file=filename, action='write', status='replace')
      write(unit, '(4a7,3a20)') 'num1', 'num2', 'denom1', 'denom2', &
          'theory', 'actual', 'err'
      do num1 = 1, nd
        do num2 = 1, nd
          do denom1 = 1, nd
            do denom2 = 1, nd
              write(unit,'(4i7,3(x,f20.8))') num1, num2, denom1, denom2, &
                  ratios(1, counter), ratios(2, counter), ratios(3, counter)
              counter = counter + 1
            end do
          end do
        end do
      end do
      close(unit)
    end do


  end subroutine write_ratios

end module FileIO
