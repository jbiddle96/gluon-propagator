module GluonOps
  use Kinds
  use MatrixOps
  use Gaugefield
  use iso_c_binding
  implicit none


  interface

    subroutine fuse_sgfix(fftw_plan)
      import :: c_long
      integer(c_long), intent(in) :: fftw_plan
    end subroutine fuse_sgfix

    subroutine fuse_plan_1d(fftw_plan, dim, in_cmplx, dir)
      use Kinds
      import :: c_long, c_int
      ! include 'latticeSize.h'
      ! integer, parameter :: nd = 4
      integer(c_long), intent(out) :: fftw_plan
      complex(DC), dimension(nt), intent(inout) :: in_cmplx
      integer(c_int) :: dim
      integer(c_int) :: dir
    end subroutine fuse_plan_1d

    subroutine fuse_plan_4d(fftw_plan, dims, in_cmplx, dir)
      use Kinds
      import :: c_long, c_int
      ! include 'latticeSize.h'
      ! integer, parameter :: nd = 4
      integer(c_long), intent(out) :: fftw_plan
      complex(DC), dimension(nx,ny,nz,nt), intent(inout) :: in_cmplx
      integer(c_int), dimension(nd) :: dims
      integer(c_int) :: dir
    end subroutine fuse_plan_4d

    subroutine fuse_destroyplan(fftw_plan)
      import :: c_long
      integer(c_long), intent(in) :: fftw_plan
    end subroutine fuse_destroyplan

  end interface

contains

  subroutine calc_Amu(UR, UI, u0)
    ! Calculate the gluon field using the usual definition of A_\mu
    ! Result overwrites values in UR and UI
    real(DP), dimension(nx,ny,nz,nt,nd,nc,nc) :: UR, UI
    real(DP), intent(in) :: u0

    ! Local vars
    real(DP), dimension(nx,ny,nz,nt,nd,nc,nc) :: glur
    real(DP), dimension(nx,ny,nz,nt,nd) :: trace
    integer :: ic, jc

    ! Calculate real and imaginary parts of A(L-mu) from the gauge links
    ! UR = real parts, UI = imaginary parts
    do ic = 1, nc
      do JC = 1, nc
        glur(:,:,:,:,:,ic,JC) = UR(:,:,:,:,:,ic,JC) - UR(:,:,:,:,:,JC,ic)
      enddo
    enddo
    UR = glur
    do ic = 1, nc
      do JC = 1, nc
        glur(:,:,:,:,:,ic,JC) = UI(:,:,:,:,:,ic,JC) + UI(:,:,:,:,:,JC,ic)
        ! Note addition not subtraction because of conjugate-transpose
      enddo
    enddo
    UI = glur

    ! Calculate trace. Only need imaginary parts as real parts are removed by conjugate-transpose
    trace = 0.d0
    do ic = 1, nc
      trace = trace + glur(:,:,:,:,:,ic,ic)
    enddo

    ! Divide trace by 3
    trace = trace / real(nc, DP)

    ! Subtract trace from imaginary part
    do ic = 1, nc
      UI(:,:,:,:,:,ic,ic) = glur(:,:,:,:,:,ic,ic) - trace
    enddo

    ! -i / 2u_0
    glur = UI / (2.d0 * u0)
    UI = -UR / (2.d0 * u0)
    UR = glur

  end subroutine calc_Amu

  subroutine calc_mom_Amu(UR, UI, momglu)
    ! Calculate the gluon field in momentum space
    real(DP), dimension(nx,ny,nz,nt,nd,nc,nc) :: UR, UI
    complex(DC), dimension(nx,ny,nz,nt,nd,nc,nc) :: momglu
    complex(DC), dimension(:,:,:,:,:,:), allocatable :: mprop_cmplx

    ! Local vars
    complex(DC), dimension(0:nx-1,0:ny-1,0:nz-1,0:nt-1) :: gluc
    integer :: ic, jc, id
    integer(c_long) :: fplan
    integer(c_int), dimension(4) :: dims
    integer(c_int) :: dir

    dims(1) = nx
    dims(2) = ny
    dims(3) = nz
    dims(4) = nt
    dir = +1

    call flushout()

    call fuse_plan_4d(fplan, dims, gluc, dir)

    ! Convert to complex type
    do id = 1, nd
      do ic = 1, nc
        do jc = 1, nc
          gluc(:, :, :, :) = cmplx(UR(:,:,:,:,id,ic,jc), &
              UI(:,:,:,:,id,ic,jc))
          ! Perform fftw, store coefficients in gluc
          call fuse_sgfix(fplan)
          momglu(:,:,:,:,id,ic,jc) = gluc(:,:,:,:)
        enddo
      enddo
    enddo
    call fuse_destroyplan(fplan)

    write(*,*) "Performed DFT"

  end subroutine calc_mom_Amu

  subroutine calc_tensor_prop(momglu, coeff, mprop)
    ! Calculate the real part of the tensor gluon propagator
    complex(DC), dimension(0:nx-1,0:ny-1,0:nz-1,0:nt-1,nd,nc,nc), intent(in) :: momglu
    real(DP), intent(in) :: coeff
    real(DP), dimension(0:NX-1, 0:NY-1, 0:NZ-1, 0:NT-1, nd, nd), intent(out) :: mprop

    ! Local vars
    complex(DC), dimension(0:NX - 1,0:NY - 1,0:NZ - 1,0:NT - 1, nd, nd) :: mprop_cmplx
    integer :: px, py, pz, pt, id, jd, npx, npy, npz, npt

    ! Calculate expectation value
    mprop_cmplx = 0.d0
    do px = 0, nx - 1
      if( px == 0 ) then
        npx = 0
      else
        npx = nx - px
      endif
      do py = 0, ny - 1
        if( py == 0 ) then
          npy = 0
        else
          npy = ny - py
        endif
        do pz = 0, nz - 1
          if( pz == 0 ) then
            npz = 0
          else
            npz = nz - pz
          endif
          do pt = 0, nt - 1
            if( pt == 0 ) then
              npt = 0
            else
              npt = nt - pt
            endif
            do id = 1, nd
              do jd = 1, nd
                mprop_cmplx(px,py,pz,pt, id, jd) &
                    = Tr(matmul(momglu(px,py,pz,pt,id, :, :), &
                    momglu(npx,npy,npz,npt,jd,:,:)))
              end do
            enddo
          enddo
        enddo
      enddo
    enddo

    mprop_cmplx = coeff * mprop_cmplx
    mprop=real(mprop_cmplx, DP)

  end subroutine calc_tensor_prop

  subroutine get_ratio_combinations(max_mom, n_comb, comb)
    integer, intent(in):: max_mom
    integer, intent(out) :: n_comb
    integer, dimension(:, :), allocatable :: comb

    ! Local vars
    integer :: ix, iy, iz, it
    integer :: counter

    ! Count number of combinations
    counter = 0
    do it = 0, max_mom
      do ix = 0, max_mom
        do iy = 0, ix
          do iz = 0, iy
            counter = counter + 1
          end do
        end do
      end do
    end do

    n_comb = counter
    if(allocated(comb)) deallocate(comb)
    allocate(comb(nd, n_comb))

    ! Assign combinations
    counter = 0
    do it = 0, max_mom
      do ix = 0, max_mom
        do iy = 0, ix
          do iz = 0, iy
            counter = counter + 1
            comb(:, counter) = [ix, iy, iz, it]
          end do
        end do
      end do
    end do

  end subroutine get_ratio_combinations

  function exact_ratio(q, num, denom, momtype) result(ratio)
    ! Calculate the exact gluon propagator ratio for a given momentum type
    integer, intent(in), dimension(nd) :: q
    integer, dimension(2), intent(in) :: num, denom
    character(len=*) :: momtype

    ! Local vars
    real(DP) :: ratio, qsq, Dmm, Dnn, term_n, term_d
    real(DP), dimension(nd) :: q_act

    select case(momtype)
    case('wil')
      q_act = wil_mom(q)
    case('lw')
      q_act = lw_mom(q)
    case default
      q_act = real(q, DP)
    end select

    if(all(q == 0)) then
      ratio = 0.0d0
      return
    end if

    qsq=sum(q_act**2)

    term_n = q_act(num(1)) * q_act(num(2)) / qsq

    term_d = q_act(denom(1)) * q_act(denom(2)) / qsq

    if(num(1) == num(2)) then
      Dmm = (1 - term_n)
    else
      Dmm = -term_n
    end if

    if(denom(1) == denom(2)) then
      Dnn = (1 - term_d)
    else
      Dnn = -term_d
    end if
    ratio = Dmm / Dnn
  end function exact_ratio

  function lw_mom(q) result(lw)
    ! Function to calculate the LW corrected momentum
    integer, dimension(nd), intent(in) :: q

    ! Loacl vars
    real(DP), dimension(nd) :: lw

    lw = 2.0d0 * sqrt( &
        sin(q_scale * q(:) / 2)**2 &
        +sin(q_scale * q(:) / 2)**4 / 3.0d0)
  end function lw_mom

  function wil_mom(q) result(wil)
    ! Function to calculate the Wilson corrected momentum
    integer, dimension(nd), intent(in) :: q

    ! Local vars
    real(DP), dimension(nd) :: wil

    wil = 2.0 * sin(q_scale * q(:) / 2)
  end function wil_mom


end module GluonOps
