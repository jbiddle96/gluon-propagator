module Gaugefield
  use Kinds
  use MatrixOps
  implicit none

contains

  subroutine Getuzero(ur, ui, uzero)
    real(DP), dimension(:,:,:,:,:,:,:), intent(in) :: ur, ui
    real(DP), intent(out) :: uzero

    ! Local vars
    complex(DC), dimension(nx,ny,nz,nt,nd,nc,nc) :: U_x
    integer :: mu, nu
    real(DP) :: plaqbar

    U_x = cmplx(ur, ui)

    plaqbar = 0.0d0

    do mu = 1, nd - 1
       do nu = mu+1, nd
          call AddPlaqBar_munu(plaqbar, U_x, mu, nu)
       end do
    end do

    uzero = (plaqbar/real(V * nplaq * nc, DP))**0.25d0

  end subroutine Getuzero

  function mappt(ii, mu)
    integer, intent(in) :: ii, mu
    integer :: mappt

    if(ii > ldims(mu)) then
       mappt = ii - ldims(mu)
    else
       mappt = ii
    end if

  end function mappt

  subroutine AddPlaqBar_munu(plaqbar, U_x, mu, nu)
    real(DP) :: plaqbar
    complex(DC), dimension(:,:,:,:,:,:,:), intent(in) :: U_x
    integer :: mu, nu

    ! Local vars
    complex(DP), dimension(nc,nc) :: UmuUnu, UmudagUnudag, plaq
    real(DP) :: plaq_x

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt

#define U_mux U_x(ix,iy,iz,it,mu,:,:)
#define U_nux U_x(ix,iy,iz,it,nu,:,:)

#define U_muxpmu U_x(jx,jy,jz,jt,mu,:,:)
#define U_nuxpmu U_x(jx,jy,jz,jt,nu,:,:)

#define U_muxpnu U_x(kx,ky,kz,kt,mu,:,:)
#define U_nuxpnu U_x(kx,ky,kz,kt,nu,:,:)

    !$ begin execution

    dmu = 0
    dnu = 0

    dmu(mu) = 1
    dnu(nu) = 1

    do it=1,nt
       jt = mappt(it + dmu(4), 4)
       kt = mappt(it + dnu(4), 4)
       do iz=1,nz
          jz = mappt(iz + dmu(3), 3)
          kz = mappt(iz + dnu(3), 3)
          do iy=1,ny
             jy = mappt(iy + dmu(2), 2)
             ky = mappt(iy + dnu(2), 2)
             do ix=1,nx
                jx = mappt(ix + dmu(1), 1)
                kx = mappt(ix + dnu(1), 1)


                UmuUnu = matmul(U_mux, U_nuxpmu)
                UmudagUnudag = matmul(ConjTrans(U_muxpnu), ConjTrans(U_nux))
                plaq = matmul(UmuUnu, UmudagUnudag)
                plaq_x = real(Tr(plaq), DP)
                ! call MultiplyMatMat(UmuUnu,U_mux,U_nuxpmu)
                ! call MultiplyMatDagMatDag(UmudagUnudag,U_muxpnu,U_nux)
                ! call RealTraceMultMatMat(plaq_x,UmuUnu,UmudagUnudag)

                plaqbar = plaqbar + plaq_x

             end do
          end do
       end do
    end do

#undef U_mux
#undef U_nux

#undef U_muxpmu
#undef U_nuxpmu

#undef U_muxpnu
#undef U_nuxpnu

  end subroutine AddPlaqBar_munu

end module Gaugefield
