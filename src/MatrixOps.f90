module MatrixOps
  use Kinds
  implicit none

  real(dp), dimension(3,3), parameter :: &
       ID3x3 = reshape((/ 1, 0, 0, 0, 1, 0, 0, 0, 1/), (/3,3/))

  interface Tr
     module procedure Tr_cplx
     module procedure Tr_real
  end interface Tr

contains 


  pure complex(dc) function Tr_cplx(mat)
    complex(dc), dimension(:,:), intent(in) :: mat
    integer :: dim, i

    dim = size(mat, 1)

    Tr_cplx = cmplx(0.0d0,0.0d0)

    do i=1,dim
       Tr_cplx = Tr_cplx + mat(i,i)
    end do
  end function Tr_cplx

  pure real(dp) function Tr_real(mat)
    real(dp), dimension(:,:), intent(in) :: mat
    integer :: dim, i

    dim = size(mat, 1)

    Tr_real = 0.0d0

    do i=1,dim
       Tr_real = Tr_real + mat(i,i)
    end do
  end function Tr_real

  pure function ConjTrans(mat)
    complex(dc), dimension(:,:), allocatable :: ConjTrans
    complex(dc), dimension(:,:), intent(in) :: mat
    integer :: dim, i, j

    dim = size(mat, 1)
    allocate(ConjTrans(dim,dim))

    do i = 1, dim
       do j = 1, dim
          ConjTrans(i,j) = Conjg(mat(j,i))
       end do
    end do

  end function ConjTrans

  
  !     *****************************************************************
  !     Uses Gram-Schmidt Orthonormalisation
  !     Algorithm adapted from mcsu3 by R.M. Woloshyn et. al.

  subroutine MAKESU3(RE, IM)
    implicit none
    integer, parameter :: ND = 4

    double precision, dimension(NX,NY,NZ,NT,ND,3,3), intent(inout) :: &
         RE, IM

    integer IC, ID
    double precision NORMR(NX,NY,NZ,NT)
    double precision NORMI(NX,NY,NZ,NT)


    do ID = 1, ND
       !     Firstly, normalise the first row

       NORMR = RE(:,:,:,:,ID,1,1)**2 + IM(:,:,:,:,ID,1,1)**2 &
            + RE(:,:,:,:,ID,1,2)**2 + IM(:,:,:,:,ID,1,2)**2 &
            + RE(:,:,:,:,ID,1,3)**2 + IM(:,:,:,:,ID,1,3)**2

       NORMR = dsqrt(NORMR)

       do IC = 1, 3
          RE(:,:,:,:,ID,1,IC) = RE(:,:,:,:,ID,1,IC) / NORMR      
          IM(:,:,:,:,ID,1,IC) = IM(:,:,:,:,ID,1,IC) / NORMR
       enddo

       !     Now compute row2 - (row2 dot row1)row1 

       NORMR = RE(:,:,:,:,ID,2,1) * RE(:,:,:,:,ID,1,1) &
            + IM(:,:,:,:,ID,2,1) * IM(:,:,:,:,ID,1,1) &
            + RE(:,:,:,:,ID,2,2) * RE(:,:,:,:,ID,1,2) &
            + IM(:,:,:,:,ID,2,2) * IM(:,:,:,:,ID,1,2) &
            + RE(:,:,:,:,ID,2,3) * RE(:,:,:,:,ID,1,3) &
            + IM(:,:,:,:,ID,2,3) * IM(:,:,:,:,ID,1,3) 

       NORMI = IM(:,:,:,:,ID,2,1) * RE(:,:,:,:,ID,1,1) &
            - RE(:,:,:,:,ID,2,1) * IM(:,:,:,:,ID,1,1) &
            + IM(:,:,:,:,ID,2,2) * RE(:,:,:,:,ID,1,2) &
            - RE(:,:,:,:,ID,2,2) * IM(:,:,:,:,ID,1,2) &
            + IM(:,:,:,:,ID,2,3) * RE(:,:,:,:,ID,1,3) &
            - RE(:,:,:,:,ID,2,3) * IM(:,:,:,:,ID,1,3)

       do IC = 1, 3
          RE(:,:,:,:,ID,2,IC) = RE(:,:,:,:,ID,2,IC) &
               - (NORMR * RE(:,:,:,:,ID,1,IC) - NORMI * IM(:,:,:,:,ID,1,IC))

          IM(:,:,:,:,ID,2,IC) = IM(:,:,:,:,ID,2,IC) &
               - (NORMR * IM(:,:,:,:,ID,1,IC) &
               + NORMI * RE(:,:,:,:,ID,1,IC))
       enddo


       !     Normalise the second row

       NORMR = RE(:,:,:,:,ID,2,1)**2 + IM(:,:,:,:,ID,2,1)**2 &
            + RE(:,:,:,:,ID,2,2)**2 + IM(:,:,:,:,ID,2,2)**2 &
            + RE(:,:,:,:,ID,2,3)**2 + IM(:,:,:,:,ID,2,3)**2

       NORMR = dsqrt(NORMR)

       do IC = 1, 3
          RE(:,:,:,:,ID,2,IC) = RE(:,:,:,:,ID,2,IC) / NORMR
          IM(:,:,:,:,ID,2,IC) = IM(:,:,:,:,ID,2,IC) / NORMR
       enddo

       !     Now generate row3 = row1 cross row2  

       RE(:,:,:,:,ID,3,3) = RE(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,2) &
            - IM(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,2) &
            - RE(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,1) &
            + IM(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,1)

       RE(:,:,:,:,ID,3,2) = RE(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,1) &
            - IM(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,1) &
            - RE(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,3) &
            + IM(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,3)

       RE(:,:,:,:,ID,3,1) = RE(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,3) &
            - IM(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,3) &
            - RE(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,2) &
            + IM(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,2)

       IM(:,:,:,:,ID,3,3) = - RE(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,2) &
            - IM(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,2) &
            + RE(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,1) &
            + IM(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,1)

       IM(:,:,:,:,ID,3,2) = - RE(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,1) &
            - IM(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,1) &
            + RE(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,3) &
            + IM(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,3)

       IM(:,:,:,:,ID,3,1) = - RE(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,3) &
            - IM(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,3) &
            + RE(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,2) &
            + IM(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,2) 

    enddo
    return
  end subroutine MAKESU3

end module MatrixOps
