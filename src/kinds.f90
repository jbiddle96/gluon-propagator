module Kinds
  implicit none
  integer, parameter  :: K10 = selected_int_kind(10)
  integer, parameter  :: SP = kind(1.0)
  integer, parameter  :: DP = kind(1.0d0)
  real(dp), parameter :: pi = 4.0*atan(1.0d0)

  integer, parameter :: sc = kind((1.0,1.0)) !! Single precision complex scalars.
  integer, parameter :: dc = kind((1.0D0,1.0D0)) !! Double precision complex scalars.

  complex(dc), parameter :: i1 = cmplx(0.0d0, 1.0d0)

  ! Lattice variables
  integer, parameter :: nc = 3
  integer, parameter :: nd = 4
  integer, parameter :: nplaq = 6
  integer, dimension(nd) :: ldims
  integer :: nx, ny, nz, nt
  integer :: V
  real(DP), dimension(nd) :: q_scale
end module Kinds
