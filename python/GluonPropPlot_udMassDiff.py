"""Plot the difference between reconstructed and original propagators"""
import matplotlib.pyplot as plt
import GPHelper as gp
import importlib
importlib.reload(gp)

# Load a config file containing a list of other config files to consider
global_config, _ = gp.load_config()

labels = []
datasets = []
for filename in global_config['files']:
    config, meta = gp.load_config(filename)
    data = gp.PrepareGPData(config, renormMethod="Indep",
                            a=meta['a'], mu=meta['mu'])
    datasets.append(data)

    m_pi = meta['m_pi']
    if m_pi is not None:
        this_label = r"$m_\pi = {}$ MeV".format(m_pi)
    else:
        this_label = 'Pure gauge'
    labels.append(this_label)

fig, ax = plt.subplots(constrained_layout=True)
for label, data in zip(labels, datasets):
    gp.MakeDiffPlot(data, label, ax, xmax=3.0)
ax.legend()
fig.savefig("GluonPropPlot_diff.pdf", bbox_inches='tight')

plt.close('all')
