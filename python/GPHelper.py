import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from scipy.optimize import curve_fit
import astropy.units as u
import natpy
import yaml
import re
import shutil
import os
import warnings

warnings.filterwarnings("error")

mpl.rc("text", usetex=True)
mpl.rc("font", family="serif")
mpl.rcParams.update({'font.size': 16})
mpl.rcParams["axes.unicode_minus"] = False
mpl.rc("text.latex", preamble=r'\usepackage{amsmath}')

default_values = {'mu': 5.5,
                  'a': 0.1,
                  'm_pi': None,
                  'N_f': 0}


class GluonPropData():
    """
    Class to store and manipulate gluon propagator data
    """
    def __init__(self, config, metadata, data=None,
                 renorm_data=None, convert_units=True):
        self.label = config['label']
        self.config = config
        self.metadata = metadata
        self.Z3 = 1.0
        self.renorm_report = {}
        self.fit_report = {}

        self._load_data(data, renorm_data=renorm_data,
                        convert_units=convert_units)

    def _load_data(self, data=None, renorm_data=None, convert_units=True):
        """Load propagator data from the file specified in self.config

        :param data: (Optional, default=None)
        Data to initialise with if config is not specified
        :param convert_units: (Optional, default=True)
        Choose whether to convert to physical units.

        """

        if data is None:
            print(f"Loading {self.config['gp_file']}")
            data = np.loadtxt(self.config['gp_file'])
        self.q = data[:, 0]
        if convert_units:
            self.q = lat_to_phys(self.q, self.metadata['a'])
        self.D = data[:, 1]
        self.D_err = data[:, 2]
        self.Z = self.D * (self.q) ** 2
        self.Z_err = self.D_err * (self.q) ** 2

        # Set intial plotting values
        if renorm_data is None:
            self.D_renorm = self.D
            self.D_err_renorm = self.D_err
            self.Z_renorm = self.Z
            self.Z_err_renorm = self.Z_err
        else:
            self.D_renorm = renorm_data[:, 1]
            self.D_err_renorm = renorm_data[:, 2]
            self.Z_renorm = self.D_renorm * (self.q) ** 2
            self.Z_err_renorm = self.D_err_renorm * (self.q) ** 2

    def update_data(self, data, renorm_data=None, convert_units=True):
        if data is not None:
            self._load_data(data, renorm_data=renorm_data,
                            convert_units=convert_units)
        else:
            raise ValueError('Cannot update data with None value')

    def apply_renorm(self, Z3=None, method=None):
        """Divide by a given renormalisation constant Z3

        :param Z3: (Optional, default=None) Renormalisation constant to apply.
        If None, a saved value corresponding to method will be applied
        :param method: (Optional, default=None) Renormalisation method,
        used as a key to save the renormalisation constant in self.renorm_report
        or to apply a previous constant

        """
        if Z3 is None:
            Z3 = self.renorm_report.get(method, None)

        if Z3 is None:
            print('No method or Z3 value provided, returning')
            return

        self.Z3 = Z3
        self.D_renorm = self.D / Z3
        self.D_err_renorm = self.D_err / Z3
        self.Z_renorm = self.Z / Z3
        self.Z_err_renorm = self.Z_err / Z3
        if method is not None:
            self.renorm_report[method] = Z3

    def fit_gp(self, fit_funcs, key, sum=False):
        """
        Perform a fit of data to functional form f

        """
        q = self.q[1:]
        D = self.D_renorm[1:]
        err = self.D_err_renorm[1:]

        fit_funcs.sum = sum
        f = fit_funcs[key]

        bounds = getattr(f, 'bounds', None)

        try:
            if bounds is not None:
                popt, pcov = curve_fit(f, q, D, sigma=err,
                                       absolute_sigma=True,
                                       bounds=bounds)
            else:
                popt, pcov = curve_fit(f, q, D, sigma=err,
                                       absolute_sigma=True)
            self.fit_report[key] = popt
            return popt
        except Exception as e:
            print(f'Fit to function {key} failed with exception:\n {e}')

    def plot(self, ax, useZ=True, fit_funcs=None, fit_key=None,
             fit_colours=None, **kwargs):
        label = kwargs.get('label', self.label)
        colour = kwargs.get('colour', self.config.get('colour', None))
        marker = kwargs.get('marker', self.config.get('marker', '.'))

        if useZ:
            y = self.Z_renorm
            err = self.Z_err_renorm
        else:
            y = self.D_renorm
            err = self.D_err_renorm

        ax.errorbar(
            self.q,
            y,
            err,
            marker=marker,
            markersize=2.5,
            elinewidth=1,
            capsize=2,
            linewidth=0,
            label=label,
            color=colour,
        )

        if fit_funcs is not None:
            q_fit = np.linspace(0.1, np.amax(self.q), 100)
            fit_params = self.fit_report[fit_key]
            result = fit_funcs(fit_key, q_fit, *fit_params, sum=False)
            if len(result) == 2:
                split = True
                ZIR_fit, ZUV_fit = result
                ZIR_fit = q_fit ** 2 * ZIR_fit
                ZUV_fit = q_fit ** 2 * ZUV_fit
                Z_fit = ZIR_fit + ZUV_fit
            else:
                split = False
                Z_fit = q_fit ** 2 * result

            if fit_colours is None:
                fit_colours = 3 * [None]

            ax.plot(q_fit, Z_fit, color=fit_colours[0], label=r"$Z(q^2)$")

            if split:
                ax.plot(q_fit, ZIR_fit, color=fit_colours[1], label=r"$Z_\text{IR}(q^2)$")
                ax.plot(q_fit, ZUV_fit, color=fit_colours[2], label=r"$Z_\text{UV}(q^2)$")

        ax.axhline(y=1.0, linestyle="--", linewidth=0.8, color="k")
        if "legend" in kwargs:
            if kwargs["legend"]:
                ax.legend(fontsize=10)
        if useZ:
            ax.set_ylabel(r"$q^2\,D(q^2)$")
        else:
            ax.set_ylabel(r"$D(q^2)~\text{GeV}^{-2}$")

        ax.set_xlabel(r"$q$ (GeV)")
        if "xmin" in kwargs:
            ax.set_xlim(left=kwargs["xmin"])
        if "xmax" in kwargs:
            ax.set_xlim(right=kwargs["xmax"])
        ymin = kwargs.get('ymin', 0)
        ax.set_ylim(bottom=ymin)
        if "ymax" in kwargs:
            ax.set_ylim(top=kwargs["ymax"])
        if "title" in kwargs:
            ax.set_title(kwargs["title"])


class FitFunctions:
    def __init__(self, consts):
        self.consts = consts
        self.functions = {'Model A': self.modelA,
                          'Model B': self.modelB,
                          'Model C': self.modelC,
                          'Gribov-Stingl': self.gribov_stingl}
        self._sum = True

        # Set default values
        self.N_f = self.consts.get('N_f', 0)
        self.xi = self.consts.get('xi', 0)

    def __call__(self, key, x, *args, sum=False):
        self._sum = sum
        result = self.functions[key](x, *args)
        self._sum = True
        return result

    def __getitem__(self, key):
        return self.functions.get(key, None)

    def __iter__(self):
        return iter(self.functions)

    def keys(self):
        return self.functions.keys()

    def values(self):
        return self.functions.values()

    def L(self, q, M):
        d_D = (39 - 9 * self.xi - 4 * self.N_f) / (2 * (33 - 2 * self.N_f))
        return (0.5 * np.log((q ** 2 + M ** 2) * (q ** (-2) + M ** (-2)))) ** (-d_D)

    def modelA(self, q, Z, A, M, alpha):
        """
        Functional form of the gluon propagator taken from Model A in
        'Asymptotic scaling and infrared behavior of the gluon propagator'
        by Leinweber et. al.

        """
        D_IR = Z * (A * M ** (2 * alpha)) / ((q ** 2 + M ** 2) ** (1 + alpha))
        D_UV = Z / (q ** 2 + M ** 2) * self.L(q, M)
        if self._sum:
            return D_IR + D_UV
        else:
            return D_IR, D_UV

    def modelB(self, q, Z, A, M, alpha):
        """
        Functional form of the gluon propagator taken from Model B in
        'Asymptotic scaling and infrared behavior of the gluon propagator'
        by Leinweber et. al.

        """
        D_IR = (
            Z * A * M ** (2 * alpha) / ((q ** 2) ** (1 + alpha) + (M ** 2) ** (1 + alpha))
        )
        D_UV = Z / (q ** 2 + M ** 2) * self.L(q, M)
        if self._sum:
            return D_IR + D_UV
        else:
            return D_IR, D_UV

    def modelC(self, q, Z, A, M, alpha):
        """
        Functional form of the gluon propagator taken from Model C in
        'Asymptotic scaling and infrared behavior of the gluon propagator'
        by Leinweber et. al.

        """
        D_IR = Z * (A / M ** 2) * np.exp(-(((q ** 2) / M ** 2) ** alpha))
        D_UV = Z / (q ** 2 + M ** 2) * self.L(q, M)
        if self._sum:
            return D_IR + D_UV
        else:
            return D_IR, D_UV

    def gribov_stingl(self, q, Z, s, M, u):
        """
        Functional form for the gluon propagatoras taken from
        'Positivity violations in QCD' by Cornwall J.

        """
        return Z * (q ** 2 + s) / (q ** 4 + 2 * u ** 2 * q ** 2 + M ** 2) * self.L(q, M)

    modelA.bounds = ([-np.inf, -np.inf, 0.0, -np.inf],
                     [np.inf, np.inf, np.inf, np.inf])

    modelB.bounds = ([-np.inf, -np.inf, 0.0, -np.inf],
                     [np.inf, np.inf, np.inf, np.inf])

    modelC.bounds = ([-np.inf, -np.inf, 0.0, -np.inf],
                     [np.inf, np.inf, np.inf, np.inf])

    gribov_stingl.bounds = ([-np.inf, -np.inf, 0.0, -np.inf],
                            [np.inf, np.inf, np.inf, np.inf])

    param_names = ['Z', 'A', 'M', 'alpha']
    modelA.param_names = param_names
    modelB.param_names = param_names
    modelC.param_names = param_names
    param_names = ['Z', 's', 'M', 'u']
    gribov_stingl.param_names = param_names


def initialise_sum_data(VR, VO, renorm=False):
    sum_gp_data = np.zeros((len(VR.q), 3))
    sum_gp_data[:, 0] = VR.q
    sum_gp_data[:, 1] = VO.D + VR.D
    sum_gp_data[:, 2] = np.sqrt(VO.D_err ** 2 + VR.D_err ** 2)

    if renorm:
        sum_gp_data_renorm = np.zeros((len(VR.q), 3))
        sum_gp_data_renorm[:, 0] = VR.q
        sum_gp_data_renorm[:, 1] = VO.D_renorm + VR.D_renorm
        sum_gp_data_renorm[:, 2] = np.sqrt(VO.D_err_renorm ** 2
                                           + VR.D_err_renorm ** 2)
        return sum_gp_data, sum_gp_data_renorm

    return sum_gp_data


def initialise_diff_data(UT, VO, renorm=False):
    diff_gp_data = np.zeros((len(UT.q), 3))
    diff_gp_data[:, 0] = UT.q
    diff_gp_data[:, 1] = UT.D - VO.D
    diff_gp_data[:, 2] = np.sqrt(UT.D_err ** 2 + VO.D_err ** 2)

    if renorm:
        diff_gp_data_renorm = np.zeros((len(UT.q), 3))
        diff_gp_data_renorm[:, 0] = UT.q
        diff_gp_data_renorm[:, 1] = UT.D_renorm - VO.D_renorm
        diff_gp_data_renorm[:, 2] = np.sqrt(UT.D_err_renorm ** 2
                                           + VO.D_err_renorm ** 2)
        return diff_gp_data, diff_gp_data_renorm

    return diff_gp_data


def renormalise(method, UT, VR, VO, sum=None):
    """Apply the specified renormalisation method
    """

    if method == 'VR':
        VR_renorm(UT, VR, VO, sum)
    elif method == 'sum':
        sum_renorm(UT, VR, VO, sum)
    elif method == 'indep':
        indep_renorm(UT, VR, VO, sum)
    elif method == 'add_const':
        add_const_renorm(UT, VR, VO, sum)
    elif method == 'indep_VRonly':
        indep_VRonly_renorm(UT, VR, VO, sum)
    elif method == 'match':
        match_renorm(UT, VR, VO, sum)
    elif method == 'UT_only':
        UT_only_renorm(UT, VR, VO, sum)
    else:
        raise ValueError(f'Unrecognised renormalise key {method}, aborting.')


def VR_renorm(UT, VR, VO, sum=None):
    """Use the VR renormalisation function as a renormaliser
    """

    UT.apply_renorm(VR.Z)
    VR.apply_renorm(VR.Z)
    VO.apply_renorm(VR.Z)
    if sum is not None:
        sum.apply_renorm(VR.Z)


def UT_only_renorm(UT, VR, VO, sum=None):

    method = 'UT_only'
    # Find UT norm
    mu = UT.metadata['mu']
    idx, _ = nearest_point(UT.q, mu)
    UT_norm = UT.Z[idx]

    UT.apply_renorm(UT_norm, method=method)
    VO.apply_renorm(UT_norm, method=method)
    VR.apply_renorm(UT_norm, method=method)
    if sum is not None:
        sum_data, renorm_data = initialise_sum_data(VR, VO, renorm=True)
        sum.update_data(sum_data, renorm_data=renorm_data, convert_units=False)


def sum_renorm(UT, VR, VO, sum=None):
    """ Renormalise VO and VR by their sum.
    """
    method = 'sum'
    if sum is None:
        sum_data = initialise_sum_data(VR, VO)
        sum = GluonPropData({'label': 'Sum'}, UT.metadata,
                            data=sum_data, convert_units=False)

    # Find UT norm
    mu = UT.metadata['mu']
    idx, _ = nearest_point(UT.q, mu)
    UT_norm = UT.Z[idx]

    # Find sum norm
    sum_norm = sum.Z[idx]

    UT.apply_renorm(UT_norm, method=method)
    VO.apply_renorm(sum_norm, method=method)
    VR.apply_renorm(sum_norm, method=method)
    sum.apply_renorm(sum_norm, method=method)


def match_renorm(UT, VR, VO, sum=None):
    """
    Renormalise by matching the VO and VR sum
    to the UT renormalisation point by normalising the VR,
    then apply the UT renormalisation to all.
    """
    method = 'match'
    # Find UT norm
    mu = UT.metadata['mu']
    idx, _ = nearest_point(UT.q, mu)
    UT_norm = UT.Z[idx]

    VR_norm = (UT.Z[idx] - VO.Z[idx]) / VR.Z[idx]

    VR.apply_renorm(1 / VR_norm, method=method)
    VO.apply_renorm(UT_norm, method=method)
    UT.apply_renorm(UT_norm, method=method)

    VR.apply_renorm((1 / VR_norm) * UT_norm)

    if sum is not None:
        sum_data, renorm_data = initialise_sum_data(VR, VO, renorm=True)
        sum.update_data(sum_data, renorm_data=renorm_data, convert_units=False)


def indep_renorm(UT, VR, VO, sum=None):
    """Renormalise VO and VR by finding a best-fit linear combination,
    subject to the renormalisation constraint at mu."""
    method = 'indep'
    x = np.array(list(zip(VO.Z, VR.Z)))
    # Find UT norm point
    mu = UT.metadata['mu']
    idx, _ = nearest_point(UT.q, mu)
    UT_norm = UT.Z[idx]

    def L(x, A):
        VO = x[:, 0]
        VR = x[:, 1]
        # Construct the VR constant such that
        # the renormalisation condition is enforced
        B = (UT_norm - A * VO[idx]) / VR[idx]
        return A * VO + B * VR

    popt, pcov = curve_fit(L, x, UT.Z,
                           sigma=UT.Z_err, absolute_sigma=True)

    A, = popt
    B = (UT_norm - A * VO.Z[idx]) / VR.Z[idx]
    # 1/A and 1/B because apply_renorm divides by constant
    VO.apply_renorm(1 / A, method=method)
    VR.apply_renorm(1 / B, method=method)

    UT.apply_renorm(UT_norm, method=method)
    VO.apply_renorm(UT_norm * (1 / A))
    VR.apply_renorm(UT_norm * (1 / B))

    if sum is not None:
        sum_data, renorm_data = initialise_sum_data(VR, VO, renorm=True)
        sum.update_data(sum_data, renorm_data=renorm_data, convert_units=False)


def indep_VRonly_renorm(UT, VR, VO, sum=None):
    """Renormalise via a best fit to the VO + const*VR"""
    method = 'indep_VRonly'
    x = np.array(list(zip(VO.Z, VR.Z)))

    def L(x, A):
        return x[:, 0] + A * x[:, 1]

    popt, pcov = curve_fit(L, x, UT.Z,
                           sigma=UT.Z_err, absolute_sigma=True)

    A, = popt
    # 1/A because apply_renorm divides by constant
    VR.apply_renorm(1 / A, method=method)

    # Find UT norm
    mu = UT.metadata['mu']
    idx, _ = nearest_point(UT.q, mu)
    UT_norm = UT.Z[idx]
    UT.apply_renorm(UT_norm, method=method)
    VO.apply_renorm(UT_norm, method=method)
    VR.apply_renorm(UT_norm * (1 / A))

    if sum is not None:
        sum_data, renorm_data = initialise_sum_data(VR, VO, renorm=True)
        sum.update_data(sum_data, renorm_data=renorm_data, convert_units=False)


def add_const_renorm(UT, VR, VO, sum=None):
    """Renormalise by adding a constant to the VO data to match the UT height"""
    method = 'add_const'
    # Find UT norm
    mu = UT.metadata['mu']
    idx, _ = nearest_point(UT.q, mu)
    UT_norm = UT.Z[idx]

    UT.apply_renorm(UT_norm, method=method)
    VR.apply_renorm(UT_norm, method=method)

    UT_height = np.amax(UT.Z_renorm) - UT.Z_renorm[idx]
    VO_height = np.amax(VO.Z) - VO.Z[idx]
    VO_norm = VO_height / UT_height

    VO.apply_renorm(VO_norm, method=method)

    if sum is not None:
        sum_data, renorm_data = initialise_sum_data(VR, VO, renorm=True)
        sum.update_data(sum_data, renorm_data=renorm_data, convert_units=False)


def nearest_point(arr, val):
    """Find the index in an array with value nearest to val
    :returns idx, minval:  The index and value at that index

    """
    idx = np.argmin(np.abs(arr - val))
    minval = arr[idx]
    return idx, minval


def lat_to_phys(data, a, type='mom'):
    """Convert data to lattice units

    :param data: Data to be converted
    :param a: Lattice spacing
    :param type: (Optional, default='mom') Converting momentum or position
    :returns data_phys: The data in physical units

    """

    a_fm = a * u.fm
    a_gev = natpy.convert(a_fm, u.GeV ** (-1)).value

    if type == 'mom':
        data_phys = data / a_gev
    elif type == 'pos':
        data_phys = data * a
    else:
        raise ValueError('Unrecognised conversion type, aborting.')
    return data_phys


def load_metadata(filename, type):

    if filename is not None:
        with open(filename, "r") as file:
            metadata = yaml.load(file, Loader=yaml.FullLoader)
    else:
        metadata = default_values
        return metadata

    if not type:
        raise ValueError('type must be specified')

    metadata = metadata[type]

    # Assign any missing keys the default value
    for key in default_values:
        metadata.setdefault(key, default_values[key])

    return metadata


def load_config(filename='config.yml'):
    """
    Load config file and return UT, VO and VR config info

    Requires:
    top level: UT, VO and VR
    gp_file: directory gontaining GP data

    optional: general, needed if you want metadata
    """
    print(f"Loading configuration file: {filename}")

    with open(filename, 'r') as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)

    general = config.pop('general', None)

    for key in config:
        config[key].setdefault('label', key)

    if general:
        meta_file = general.get('meta_file')
        ensemble_type = general.get('type')
        metadata = load_metadata(meta_file, ensemble_type)
        for key in metadata:
            general[key] = metadata[key]

    return config, general


def assign_dir(dir_name, clean=False):
    """Assign a directory and ensure it exists

    :param dir_name: The directory to assign
    :param clean: (Optional, default=False) Empty the directory
    :returns dir_name:

    """
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    if clean:
        for filename in os.listdir(dir_name):
            file_path = os.path.join(dir_name, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

    return dir_name


def load_gluprop_data(config_info, metadata, make_sum=False, make_diff=False):
    """Load the gluon propagator ensembles specified in config_info.
    Optionally load sum and diff data.

    :param config_info: Dictionary containing configuration info
    :param metadata: Ensemble metadata
    :param make_sum: (Optional, default=False) construct a summed data object
    :param make_diff: (Optional, default=False) construct a difference data object
    :returns: tuple of all constructed data objects

    """
    UT = GluonPropData(config_info['UT'], metadata)
    VR = GluonPropData(config_info['VR'], metadata)
    VO = GluonPropData(config_info['VO'], metadata)

    result = [UT, VR, VO]

    if make_sum:
        sum_data = initialise_sum_data(VR, VO)
        sum = GluonPropData(config_info['Recon'], metadata,
                            data=sum_data, convert_units=False)
        result.append(sum)

    if make_diff:
        diff_config = {'label': 'diff'}
        diff_data = initialise_diff_data(UT, VO)
        diff = GluonPropData(diff_config, metadata,
                             data=diff_data,
                             convert_units=False)
        result.append(diff)

    return tuple(result)



def load_ratio_data(directory, maxmom=None):
    """Load gluon propagator ratio data from directory

    Filenames must have extension ".dat"
    and contain the 4-momentum in the filename with a substring "q****"

    :param directory: Directory to load data from
    :returns data: A dictionary of ratio data keyed by a tuple of the momentum.

    """

    tp = np.dtype([('num1', int),
                   ('num2', int),
                   ('denom1', int),
                   ('denom2', int),
                   ('theory', float),
                   ('actual', float),
                   ('err', float)])
    pattern = os.path.join(directory, '*.dat')
    files = sorted(glob(pattern))
    pattern = re.compile('\S*q(\d)(\d)(\d)(\d)\S*.dat')
    data = {}
    for file in files:
        match = re.search(pattern, file)
        if match:
            q = tuple([int(match.group(i)) for i in range(1, 5)])
        else:
            raise ValueError('Ratio filename did not conform to regex pattern')

        # Cycle if any momenta are greater than the max
        if maxmom is not None:
            if any(i > maxmom for i in q):
                continue

        filepath = os.path.join(directory, file)
        data[q] = np.loadtxt(filepath, dtype=tp, skiprows=1)

    return data


def jackknife(data, labels):
    """
    Jackknife the data.
    Each row of data is a single sample. Jackknifing is done over the columns
    """
    n_jack = len(data)
    data = np.array(data)
    mean = []
    sigma = []
    for i, col in enumerate(data.T):
        this_mean = np.mean(col)
        jack_samples = (n_jack * this_mean - col) / (n_jack - 1)
        mean.append(this_mean)
        this_sigma = np.sqrt((n_jack - 1) * np.mean((jack_samples - this_mean) ** 2))
        sigma.append(this_sigma)

    result = {labels[0]: np.arange(len(mean)),
              labels[1]: np.array(mean),
              labels[2]: np.array(sigma)}
    return result


def MakeDiffPlot(data, label, ax, makeAbs=True, **kwargs):
    q = data["UT"][:,0]
    if(makeAbs):
        dataDiff = np.abs(q**2 * (data["UT"][:,1] - data["Recon"][:,1]))
    else:
        dataDiff = q**2 * (data["UT"][:,1] - data["Recon"][:,1])

    dataDiffErr = q**2 * np.sqrt((data["UT"][:,2]**2 + (data["Recon"][:,2])**2))

    ax.errorbar(q, dataDiff, dataDiffErr,
                marker=".", markersize=2.5,
                elinewidth=1, capsize=2,
                linewidth=0, label=label)

    if('xmin' in kwargs):
        ax.set_xlim(left=kwargs['xmin'])
    if('xmax' in kwargs):
        ax.set_xlim(right=kwargs['xmax'])
    if('ymin' in kwargs):
        ax.set_ylim(bottom=kwargs['ymin'])
    if('ymax' in kwargs):
        ax.set_ylim(top=kwargs['ymax'])

    ax.axhline(color='black', ls='--')
    if(makeAbs):
        ax.set_ylabel(r"$\lvert D_\text{UT}(q^2)$ - $D_\text{Recon}(q^2)\lvert$")
    else:
        ax.set_ylabel(r"$D_\text{UT}(q^2)$ - $D_\text{Recon}(q^2)$")
    ax.set_xlabel(r"$q$ (GeV)")


def MergePoints(x, y, yErr, windowSize=0.005):
    """
    Perform a weighted average to merge y points
    within Δx = windowSize of each other.
    Weights are given by 1/yErr^2.
    Errors for the new points are added in quadrature

    Inputs:
    -------
    x - Array of x coordinates to be merged
    y - Array of y coordinates to be merged
    yErr - Standard deviations (assumed normal) on each y coordinate
    windowSize - x window in which to merge points

    Returns:
    --------
    out - Numpy array containing the 3 merged arrays
    """
    x_merge = np.array([])
    y_merge = np.array([])
    yErr_merge = np.array([])
    i = 0
    while i < (len(x) - 1):
        x_window = np.array([x[i]])
        y_window = np.array([y[i]])
        yErr_window = np.array([yErr[i]])
        j = i + 1
        while np.abs(x[j] - x[i]) < windowSize:
            x_window = np.append(x_window, x[j])
            y_window = np.append(y_window, y[j])
            yErr_window = np.append(yErr_window, yErr[j])
            if j < (len(x) - 1):
                j += 1
            else:
                break

        i = j
        x_new = np.mean(x_window)
        weights = 1 / (yErr_window ** 2)
        y_new = np.sum(y_window * weights) / np.sum(weights)
        yErr_new = np.sqrt(np.sum(yErr_window ** 2))

        x_merge = np.append(x_merge, x_new)
        y_merge = np.append(y_merge, y_new)
        yErr_merge = np.append(yErr_merge, yErr_new)

    out = np.array([x_merge, y_merge, yErr_merge])
    return out


def save_renorm_data(filename, UT, VR, VO, sum):
    datasets = [UT, VR, VO, sum]
    method_sets = [set(d.renorm_report.keys()) for d in datasets]
    methods = method_sets[0].union(*method_sets)

    method_dict = {}
    for method in methods:
        method_dict[method] = {d.label: d.renorm_report.get(method, None)
                               for d in datasets}
        for key in method_dict[method]:
            if method_dict[method][key] is not None:
                method_dict[method][key] = float(method_dict[method][key])

    with open(filename, "w") as stream:
        yaml.dump(method_dict, stream, default_flow_style=False)


def save_fit_data(filename, UT, fit_funcs):
    fit_dict = {}
    for key in UT.fit_report:
        param_names = getattr(fit_funcs[key], 'param_names', None)
        if param_names is None:
            param_names = [f'Param {i}' for i in range(len(UT.fit_report[key]))]
        report = {p: v for p, v in zip(param_names, UT.fit_report[key])}
        for rkey in report:
            report[rkey] = float(report[rkey])
        fit_dict[key] = report

    with open(filename, "w") as stream:
        yaml.dump(fit_dict, stream, default_flow_style=False)


def update_bounds(old, new):
    """Update lower and upper bounds with new values"""

    if (len(old) != 2) or (len(new) != 2):
        raise ValueError('Both arguments must be of length 2')

    low, high = old
    new_low, new_high = new

    if new_low < low:
        low = new_low

    if new_high > high:
        high = new_high

    return low, high
