"""Script to analyse gluon proapagator tensor structure"""
import GPHelper as gp
import importlib
import os
import itertools as it
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
importlib.reload(gp)

default_width = 6.4
default_height = 4.8


def plot_ratio_data(data, output_dir, numtype='diag', denomtype='diag',
                    theory_diff=False):
    """Plot gluon propagator ratio data for all momenta

    Use numtype and denomtype to specify the types of ratios to plot

    :param data: Dictionary of ratio data a different momenta
    :param output_dir: Output directory for the plots
    :param numtype: (Optional, default='diag)
    Toggles between 'diag' and 'offdiag' numerator
    :param denomtype: (Optional, default='diag)
    Toggles between 'diag' and 'offdiag' denominator
    :param theory_diff: (Optional, default=False)
    Toggle whether to plot the raw data or the difference to theory.
    """
    for q in data:
        qstring = ''.join(str(i) for i in q)
        momdata = data[q].copy()
        if numtype == 'diag':
            num_mask = (momdata['num1'] == momdata['num2'])
            numlabel = r'\mu,\mu'
        elif numtype == 'offdiag':
            num_mask = (momdata['num1'] != momdata['num2'])
            numlabel = r'\mu,\nu'
        else:
            raise ValueError('Unrecognised numtype')

        if denomtype == 'diag':
            denom_mask = (momdata['denom1'] == momdata['denom2'])
            denomlabel = r'\rho,\rho'
        elif denomtype == 'offdiag':
            denom_mask = (momdata['denom1'] != momdata['denom2'])
            denomlabel = r'\rho,\sigma'
        else:
            raise ValueError('Unrecognised denomtype')

        # Extract the data
        mask = (num_mask & denom_mask)
        if theory_diff:
            mask = mask & np.isfinite(momdata['theory'])
        momdata = momdata[mask]

        # Split data by diagonal component
        data_series = []
        if numtype == 'diag':
            split_on = 'num'
            axtitle = numlabel
            unique_num = np.unique(momdata[['num1', 'num2']])
            for num in unique_num:
                data_series.append(momdata[momdata[['num1', 'num2']] == num])
        elif denomtype == 'diag':
            split_on = 'denom'
            axtitle = denomlabel
            unique_denom = np.unique(momdata[['denom1', 'denom2']])
            for denom in unique_denom:
                data_series.append(momdata[momdata[['denom1', 'denom2']] == denom])
        else:
            split_on = None
            axtitle = ''
            data_series.append(momdata)

        if theory_diff:
            filename = f'q{qstring}-{numtype}-{denomtype}-diff.pdf'
        else:
            filename = f'q{qstring}-{numtype}-{denomtype}.pdf'
        filename = os.path.join(output_dir, filename)

        # Plot the ratio data
        fig, ax = plt.subplots(figsize=[1.5 * default_width, default_height])
        for i, series in enumerate(data_series):
            if split_on == 'num':
                x = [f'$({numlabel})/({d1}, {d2})$' for (d1, d2)
                     in series[['denom1', 'denom2']]]
                label = np.unique(series[['num1', 'num2']])[0]
            elif split_on == 'denom':
                x = [f'$({n1}, {n2})/({denomlabel})$' for (n1, n2)
                     in series[['num1', 'num2']]]
                label = np.unique(series[['denom1', 'denom2']])[0]
            else:
                x = [f'({n1}, {n2})/({d1}, {d2})' for (n1, n2, d1, d2)
                     in series[['num1', 'num2', 'denom1', 'denom2']]]
                label = None

            y = series['actual']
            y_err = series['err']
            if theory_diff:
                y = y - series['theory']

            offset = lambda p: transforms.ScaledTranslation(p/72.,0,
                                                            plt.gcf().dpi_scale_trans)
            trans = plt.gca().transData

            ax.errorbar(
                x,
                y,
                y_err,
                marker=".",
                markersize=2.5,
                elinewidth=1,
                capsize=2,
                linewidth=0,
                label=label,
                transform=trans+offset(i * 4)
            )

        plt.setp(ax.get_xticklabels(), rotation=45, ha='right')

        if theory_diff:
            ax.set_ylabel(f'$D_{{{numlabel}}}/D_{{{denomlabel}}} - ' +
                          f'D^\\text{{theory}}_{{{numlabel}}}/D^\\text{{theory}}_{{{denomlabel}}}$')
            ax.axhline(color='black', linestyle='dashed')
        else:
            ax.set_ylabel(f'$D_{{{numlabel}}}/D_{{{denomlabel}}}$')
            # Add hline for q=0 offdiag/diag plots
            if ((numtype == 'offdiag')
                and (denomtype == 'diag')
                and all(i == 0 for i in q)):
                ax.axhline(y=0, color='black', linestyle='dashed')

        # ax.legend()
        # Shrink current axis by 20%
        # box = ax.get_position()
        # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        handles, labels = ax.get_legend_handles_labels()
        if handles:
            ax.legend(loc='center left',
                      bbox_to_anchor=(1, 0.5),
                      title=f'$({axtitle})$')

        fig.tight_layout()
        plt.savefig(filename)
        plt.close(fig)
        print(f"Saved {filename}")


if __name__ == "__main__":
    config_info, _ = gp.load_config()
    mpl.rcParams.update({'font.size': 18})

    for key in config_info:
        print(f'Plotting {key} data')
        cfg = config_info[key]
        ratio_dir = cfg['directory']
        maxmom = cfg.get('maxmom', None)
        clean = cfg.get('clean', False)
        data = gp.load_ratio_data(ratio_dir, maxmom)
        plotdir = gp.assign_dir(f'./{key}_plots', clean)

        for ntype, dtype in it.product(['diag', 'offdiag'], repeat=2):
            plot_ratio_data(data, plotdir, ntype, dtype)
            plot_ratio_data(data, plotdir, ntype, dtype, theory_diff=True)
