import matplotlib.pyplot as plt
import GPHelper as gp
import importlib
importlib.reload(gp)

if __name__ == '__main__':
    global_config, _ = gp.load_config()

    data_dict = {}
    for key in global_config:
        config = global_config[key]
        filename = config['config_file']
        config, meta = gp.load_config(filename)
        UT, VR, VO = gp.load_gluprop_data(config,
                                         meta)
        data_dict[key] = {'UT': UT,
                          'VR': VR,
                          'VO': VO}

    methods = ['indep', 'sum', 'UT_only']

    for method in methods:
        print(f'Performing {method} renormalisation')
        fig_UT, ax_UT = plt.subplots(constrained_layout=True)
        fig_VR, ax_VR = plt.subplots(constrained_layout=True)
        fig_VO, ax_VO = plt.subplots(constrained_layout=True)

        for key in data_dict:
            UT = data_dict[key]['UT']
            VR = data_dict[key]['VR']
            VO = data_dict[key]['VO']
            gp.renormalise(method, UT, VR, VO)

            pion_mass = UT.metadata['m_pi']
            if pion_mass is not None:
                label = r"$m_\pi = {}$ MeV".format(pion_mass)
            else:
                label = "Pure gauge"

            UT.plot(ax_UT, legend=True, ymax=3.0, label=label, colour=None)
            VR.plot(ax_VR, legend=True, ymax=3.0, label=label, colour=None)
            VO.plot(ax_VO, legend=True, ymax=3.0, label=label, colour=None)

        fig_UT.savefig(f"UT_gluprop_{method}_renorm.pdf", bbox_inches='tight')
        fig_VR.savefig(f"VR_gluprop_{method}_renorm.pdf", bbox_inches='tight')
        fig_VO.savefig(f"VO_gluprop_{method}_renorm.pdf", bbox_inches='tight')
