import matplotlib.pyplot as plt
import GPHelper as gp
import importlib
import numpy as np
importlib.reload(gp)


if __name__ == '__main__':
    config_info, metadata = gp.load_config()

    pion_mass = metadata['m_pi']
    maketitle = metadata.get('make_title', False)
    if maketitle:
        if pion_mass is not None:
            title = r"$m_\pi = {}$ MeV".format(pion_mass)
        else:
            title = "Pure gauge"
    else:
        title = None

    UT, VR, VO, sum, diff = gp.load_gluprop_data(config_info,
                                                 metadata,
                                                 make_sum=True,
                                                 make_diff=True)

    # |-------------------- Renormalisation methods ---------------------|

    methods = ['indep', 'sum', 'UT_only']
    datasets = [UT, VR, VO]
    for method in methods:
        print(f'Performing {method} renormalisation')
        gp.renormalise(method, UT, VR, VO, sum)

        diff_data, diff_data_renorm = gp.initialise_diff_data(UT, VO, renorm=True)
        diff.update_data(diff_data, diff_data_renorm, convert_units=False)

        fig_D, ax_D = plt.subplots(constrained_layout=True)
        fig_Z, ax_Z = plt.subplots(constrained_layout=True)
        fig_diff, ax_diff = plt.subplots(constrained_layout=True)

        if method != 'UT_only':
            plot_set = datasets + [sum]
        else:
            plot_set = datasets

        for dataset in plot_set:
            dataset.plot(ax_Z, legend=True, ymax=3.0,
                         title=title)
            dataset.plot(ax_D, useZ=False, legend=True, xmax=2.0,
                         title=title)
            dataset.plot(ax_diff, legend=True, ymax=3.0,
                         title=title)

        diff.plot(ax_diff, legend=True, ymax=3.0,
                  title=title)

        fig_Z.savefig(f"GluonPropPlot_{method}_renorm.pdf",
                      bbox_inches='tight')
        fig_D.savefig(f"GluonPropPlot_{method}_renorm_rawDq2.pdf",
                      bbox_inches='tight')
        fig_diff.savefig(f"GluonPropPlot_{method}_renorm_diff.pdf",
                         bbox_inches='tight')

    gp.save_renorm_data("RenormData.yml", *(datasets + [sum]))

    # |---------------------------- Fitting -----------------------------|

    consts = {'N_f': metadata.get('N_f', 0),
              'xi': metadata.get('xi', 0)}
    fit_funcs = gp.FitFunctions(consts)

    print('Fitting propagator')
    fitParamsA = UT.fit_gp(fit_funcs, 'Model A', sum=True)
    fitParamsB = UT.fit_gp(fit_funcs, 'Model B', sum=True)
    fitParamsC = UT.fit_gp(fit_funcs, 'Model C', sum=True)
    fitParamsGS = UT.fit_gp(fit_funcs, 'Gribov-Stingl')

    fitParams = [fitParamsA, fitParamsB, fitParamsC, fitParamsGS]
    fitFuncs = fit_funcs.keys()
    colours = [data.config.get('colour', None) for data in [UT, VO, VR]]

    for method in ['indep', 'sum']:
        fig, axes = plt.subplots(ncols=2, nrows=2, constrained_layout=True,
                                 figsize=[12.8, 4.8])

        UT.apply_renorm(method=method)
        VO.apply_renorm(method=method)
        VR.apply_renorm(method=method)

        for key, ax in zip(fitFuncs, axes.flatten()):
            UT.plot(ax, fit_funcs=fit_funcs, fit_key=key, fit_colours=colours)
            VR.plot(ax)
            VO.plot(ax)
            ax.set_title(key)

        axes.flatten()[-1].legend(fontsize=10)

        fig.savefig(f"GluonPropPlot_Fitted_{method}.pdf", bbox_inches='tight')
        plt.close('all')

    gp.save_fit_data('FitData.yml', UT, fit_funcs)

    print('Done')
