import numpy as np
from numpy.fft import fft
import matplotlib.pyplot as plt
import glob
import os
import GPHelper as gp
import importlib
importlib.reload(gp)


def load_propagator(filename, apply_0_norm=False):
    """Load the propagator file"""
    prop = np.loadtxt(filename, skiprows=5)
    prop = prop[:-1]
    if apply_0_norm:
        prop[0] *= (3 / 4)
    return prop


def save_correlator(filename, data_dict):
    """Save the correlator"""
    with open(filename, 'w+') as f:
        for t, C, C_err in zip(data_dict['t'], data_dict['C'], data_dict['C_err']):
            f.write(f'{t:15d} {C:15f} {C_err:15f}\n')


def fft_propagator(prop):
    N = len(prop)
    corr = fft(prop) / N
    # Euclidean correlator has no imaginary part
    corr = np.real(corr[:N // 2])
    return corr


def make_plot(config, general, data_key, out_name, units='physical'):
    """Generate the correlator plot

    :param config: Configuration info
    :param general: Metadata dictionary
    :param data_key: What type of data we're plotting for each config
    :param out_name: Output plot name
    :param units: (Optional, default='physical') Select between 'physical' and 'lattice' units

    """
    fig, ax = plt.subplots(constrained_layout=True)
    ymin, ymax = np.inf, -np.inf
    nt = general['nt']
    xmin = general.get('xmin', 0)
    xmax = general.get('xmax', nt // 2)
    if units == 'physical':
        a = general['a']
        xmin = gp.lat_to_phys(xmin, a, type='pos')
        xmax = gp.lat_to_phys(xmax, a, type='pos')
        xlabel = r"$t~(\text{fm})$"
    else:
        xlabel = r"$t$"

    for i, key in enumerate(config):
        cfg = config[key]
        t = cfg[data_key]['t']

        if units == 'physical':
            t = gp.lat_to_phys(t, a, type='pos')

        C = cfg[data_key]['C']
        C_err = cfg[data_key]['C_err']
        range_mask = (t >= xmin)
        C_range = C[range_mask]
        C_err_range = C_err[range_mask]

        idx = np.argmin(C_range)
        new_ymin = np.amin(C_range) - C_err_range[idx]

        idx = np.argmax(C_range)
        new_ymax = np.amax(C_range) + C_err_range[idx]
        ymin, ymax = gp.update_bounds((ymin, ymax), (new_ymin, new_ymax))

        ax.errorbar(
            t,
            C,
            C_err,
            markersize=2.5,
            elinewidth=1,
            capsize=2,
            linewidth=0,
            color=cfg.get('colour'),
            label=cfg.get('label'),
            marker=cfg.get('marker')
        )

    ymin = ymin - 0.1 * abs(ymin)
    ymax = ymax + 0.1 * abs(ymax)

    # Overrule ymax/ymin if so desired
    ymax = general.get('ymax', ymax)
    ymin = general.get('ymin', ymin)

    ax.legend()
    ax.axhline(xmax=xmax, color="black", ls="--")
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(r"C(t)")

    plt.savefig(out_name)
    print(f'Saved {out_name}')


if __name__ == "__main__":
    config, general = gp.load_config('config.yml')

    plot_dir = './EuclC_plots/'
    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)

    points_dir = './EuclC_points/'
    if not os.path.exists(points_dir):
        os.makedirs(points_dir)

    for key in config:
        cfg = config[key]
        pattern = os.path.join(cfg['dir'], "*.TempProp")
        file_list = sorted(glob.glob(pattern))
        max_files = cfg.get('max_files', len(file_list))
        corr = []
        prop = []
        corr_0_norm = []
        prop_0_norm = []
        corr_half = []
        for fname in file_list[:max_files]:
            this_prop = load_propagator(fname)
            prop.append(this_prop)
            corr.append(fft_propagator(this_prop))
            this_prop_0_norm = load_propagator(fname, apply_0_norm=True)
            prop_0_norm.append(this_prop_0_norm)
            corr_0_norm.append(fft_propagator(this_prop_0_norm))

        labels = ['t', 'C', 'C_err']
        cfg['prop'] = gp.jackknife(prop, labels)
        cfg['corr'] = gp.jackknife(corr, labels)
        cfg['prop_0_norm'] = gp.jackknife(prop_0_norm, labels)
        cfg['corr_0_norm'] = gp.jackknife(corr_0_norm, labels)
        general['nt'] = len(cfg['prop']['t'])

        out_name = os.path.join(points_dir, f'{cfg["label"]}_correlator.dat')
        save_correlator(out_name, cfg['corr'])
        out_name = os.path.join(points_dir, f'{cfg["label"]}_correlator_0_norm.dat')
        save_correlator(out_name, cfg['corr_0_norm'])
        print(f'Analysed {cfg["label"]} data')

    print("Plotting")

    # Make correlator plot
    filename = os.path.join(plot_dir, 'EuclC_combined.pdf')
    make_plot(config, general, 'corr', filename)
    filename = os.path.join(plot_dir, 'EuclC_0_norm_combined.pdf')
    make_plot(config, general, 'corr_0_norm', filename)

    # Make t-propagator plot
    filename = os.path.join(plot_dir, 'PropT_combined.pdf')
    make_plot(config, general, 'prop', filename)
    filename = os.path.join(plot_dir, 'PropT_0_norm_combined.pdf')
    make_plot(config, general, 'prop_0_norm', filename)

    plt.close('all')
    print("Done")
